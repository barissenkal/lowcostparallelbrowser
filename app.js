var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var debug = require('debug')('lcccmb:app');
var fs = require('fs');

var routes = require('./routes/index');
var api = require('./routes/api');

var app = express();


/**
* For index.html/#/updates to work
*/

var updatePostsList = function(){
  fs.readdir("./public/posts", function(err, items) {
    
    posts = [];
    
    items.map(function(x){
      x.endsWith(".html") && posts.push(x.slice(0,-5));
    });
    
    posts.reverse();
    
    fs.writeFile("./public/posts.json", JSON.stringify(posts), 'utf8', function(){
      debug("posts",posts);
    });
    
  });
}
updatePostsList();

//Updating posts if content changes.
fs.watch("./public/posts", { persistent: true }, function (event, fileName) {
  updatePostsList();
    debug("Event: " + event,fileName + "\n");
});




// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser()); //TODO check if needed
app.use(express.static(path.join(__dirname, 'public')));

// Adding CORS headers
app.use(function (req, res, next) {

    // Website to allow to connect. //TODO make it every single website.
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use('/', routes);
app.use('/api', api);

// catch 404
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
  	debug(err);
   res.status(err.status || 500).send({ error: err });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500).send({ error: "Ooops." }); //TODO change :)
});


module.exports = app;
