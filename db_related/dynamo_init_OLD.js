db.init = function(conf,callback) {
	console.log("init",conf);
	
	/*
	    jobs table √
	    groups table √
	    results table √
	    
	    TODO: keep track of clients ( +workers) and jobs in another table.
	    
	    Maybe:
	    oddParts table?
	    unfinished (Job Data) table ? : No need, can keep multiple data in future. Or seperate DB for data and just IDs in table.
	    
	    Table for side info?
	    	resultFuncs, jobSizes
	     
	 */
	 
	var dynamodb = new AWS.DynamoDB();
	 
	dynamodb.listTables({}, function(err, data) {
		if (err){
			console.log(JSON.stringify(err, null, 2));
			return;
		}
		
		console.log("Tables in DB:" + JSON.stringify(data, null, 2));
		
		if(!data || ! "TableNames" in data || data["TableNames"].length == 0) {
			
			var tables = [
				{
					TableName: "Jobs",
					KeySchema: [
						{ AttributeName: "jid", KeyType: "HASH"},  //Partition key
						//{ AttributeName: "priority", KeyType: "RANGE" }  //Sort key
					],
					AttributeDefinitions: [
						{
							AttributeName: 'jid',
							AttributeType: 'S'
						},
						// {
						// 	AttributeName: 'jobType',
						// 	AttributeType: 'S'
						// },
						{
							AttributeName: 'priority',
							AttributeType: 'N'
						},
						// {
						// 	AttributeName: 'status',
						// 	AttributeType: 'N'
						// },
						// {
						// 	AttributeName: 'partsLeft',
						// 	AttributeType: 'N'
						// }
					],
					GlobalSecondaryIndexes: [{
						IndexName: "priority-index",
						KeySchema: [
							{
								AttributeName: "jid",
								KeyType: "RANGE"//HASH
							},
							{
								AttributeName: "priority",
								KeyType: "HASH"//RANGE
							}
						],
						Projection: {
							ProjectionType: "ALL"//"KEYS_ONLY"
						},
						ProvisionedThroughput: {
							ReadCapacityUnits: 1,
							WriteCapacityUnits: 1
						}
					}
					],
					ProvisionedThroughput:  {
						ReadCapacityUnits: 1,
						WriteCapacityUnits: 1
					}
				},
				{
					TableName: "Groups",
					KeySchema: [
						{ AttributeName: "group", KeyType: "HASH"},  //Partition key
						{ AttributeName: "part", KeyType: "RANGE" }  //Sort key
					],
					AttributeDefinitions: [
						{
							AttributeName: 'group',
							AttributeType: 'S'
						},
						{
							AttributeName: 'part',
							AttributeType: 'S'
						}
					],
					ProvisionedThroughput:  {
						ReadCapacityUnits: 2,
						WriteCapacityUnits: 2
					}
				},
				{
					TableName: "Results",
					KeySchema: [
						{ AttributeName: "resultName", KeyType: "HASH"},  //Partition key
						{ AttributeName: "id", KeyType: "RANGE" }  //Sort key
					],
					AttributeDefinitions: [
						{
							AttributeName: 'resultName',
							AttributeType: 'S'
						},
						{
							AttributeName: 'id',
							AttributeType: 'S'
						}
					],
					ProvisionedThroughput:  {
						ReadCapacityUnits: 2,
						WriteCapacityUnits: 2
					}
				}
			];
		
			var j = tables.length;
			for (var i = 0; i < tables.length; i++) {
				dynamodb.createTable(tables[i], function(err, data) {
					if (err) {
						console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
					} else {
						console.log("Table description JSON:", JSON.stringify(data, null, 2));
					}
					
					if(--j == 0 && callback) callback();
					
				});
			}
			
		} else {
			if(callback) callback();
			
		}
		
	});
}