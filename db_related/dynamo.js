var debug = require('debug')('lcccmb:db');

var AWS = require("aws-sdk");

//TODO switch from local DynamoDB to Amazon hosted with "DBURL" enviroment var.

AWS.config.update({
  region: process.env.DBREG || "eu-west-1",
  endpoint: process.env.DBURL || "https://dynamodb.eu-west-1.amazonaws.com"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var db = {};

db.name = "DynamoDB";

//TODO don't use this, use random + background job instead.
//Used for new reduce jobs.
//var localOddEven = 0;

var priorityLimit = 1000000;

//TODO global-index for priority should short by priority (It is still shorting by jid)

//TODO background progress for reseting unfinished datajobs.
//TODO save time when giving jobs, then loop through jobs that are in progress and reset ones that didn't finish yet.


//TODO save this and load back from Config Table
var GroupSizes = {};
var resultFuncs = {};
var submissionSizes = {};


function randomID(length) {
	
	var str = new Array(length + 1).join("x");
	
	//gnu id generator
	return str.replace(/[xy]/g, function (c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}



db.throwReaderJobsIn = function(conf,callback){
	console.log("throwReaderJobsIn",conf);
	
	if(!(conf.subID in submissionSizes)) submissionSizes[conf.subID] = 0;
	submissionSizes[conf.subID] += conf.size;
	
	var reqs = [];
	
	for (var i = 0; i < conf.size; i++) {
		reqs.push({  
			PutRequest: {
				Item: {
					"jobType":"read",
					"jid": [conf.subID,conf.func + "-" + i].join("="),
					
					"priority":3* priorityLimit - conf.priority,
					
					"status":0,
					"partsLeft":0,
					"valuesSent":[],
					
					"json":conf.json, //TODO option to fill from array of json urls
					"group":conf.group,
					"size":conf.size, //Is there a better way to do it?
					
				}
			}
		});
		
		//TODO do this in parts if size is too big.
	}
	
	var params = {
		RequestItems: {
			"Jobs": reqs
		}
	};

	docClient.batchWrite(params, function (err, data) {
		if (err)
			console.log(JSON.stringify(err, null, 2));
		else
			console.log(JSON.stringify(data, null, 2));
		
		//TODO check UnprocessedItems items in data.
		
		if(callback) callback();
		
	});
	
	
};



//TODO use db's config table
var globalCheckedParts = {};

var reduceJobResultNames = [];

//TODO add reduce jobs from background job
db.addReduceJobs = function() {
	
	if(reduceJobResultNames.length <= 0) return;
		
	var params = {
		TableName: "Results",
		//FilterExpression: "resultName = :rn",
		ProjectionExpression: "#s,id,resultName",
		FilterExpression: "#s = :zero",
		ExpressionAttributeNames:{
		    "#s":"status"
		},
		ExpressionAttributeValues: {
			//":rn": resultName,
			":zero": 0
		},
		ConsistentRead: true
	};

	var count = {};
	var countStatusZero = {};
	var newParts = {};
	for (var resulName in resultFuncs) {
		count[resulName] = 0;
		countStatusZero[resulName] = 0;
		newParts[resulName] = [];
	}

	docClient.scan(params, onScan);

	function onScan(err, data) {
		if (err) {
			console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
		} else {
			
			data.Items.forEach(function(item) {
				
				//console.log("find-me "+JSON.stringify(globalCheckedParts, null ,2) + " " + item.resultName);
				
				if(!(item.resultName in resultFuncs)) return;
				
				countStatusZero[item.resultName]++;
				if(globalCheckedParts[item.resultName].indexOf(item.id) < 0){
					count[item.resultName]++;
					newParts[item.resultName].push(item.id);	
				}
				
			});

			// continue scanning if we have more movies, because
			// scan can retrieve a maximum of 1MB of data
			if (typeof data.LastEvaluatedKey != "undefined") {
				params.ExclusiveStartKey = data.LastEvaluatedKey;
				docClient.scan(params, onScan);
			} else {
				
				//Finished scanning.
				
				// if(countStatusZero == 1){
				// 	console.log("Result ready?:"+resultName);
					
				// 	//TODO do it in config db.
				// 	reduceJobResultNames = reduceJobResultNames.filter(function (item) {
				// 		return item != resultName;
				// 	});
					
				// }
				
				reduceJobResultNames.forEach(function (resultName) {
				
					if(!(count[resultName] > 1))  return;
					
					console.log("addReduceJobs count[resultName]="+count[resultName]+" "+ JSON.stringify(newParts[resultName]));
					
					var index = 0;
					
					while (count[resultName] > 1) {
						
						count[resultName] -= 2;
						
						var partID1 = newParts[resultName][index++];
						var partID2 = newParts[resultName][index++];
						
						globalCheckedParts[resultName].push(partID1);
						globalCheckedParts[resultName].push(partID2);
						
						console.log("added to globalCheckedParts["+resultName+"]:"+partID1+" "+partID2);
						
						var subIDNrealResultName = resultName.split("=");
						var subID = subIDNrealResultName[0];
						var realResultName = subIDNrealResultName[1];
						
						var params2 = {
							TableName: "Jobs",
							Item: {
								"jobType":"reduce",
								
								//TODO maybe: use IDs when adding the job and get result parts with IDs ???
								"jid": (function() {
									var jid = subID + "=" + resultFuncs[resultName] + "-" + partID1 + "-" + partID2;
									console.log("addReduceJobs jid= " + jid);
									return jid;
								})(),
								
								"priority": 3 * priorityLimit - 100, //TODO based on resultName
								
								"status":0,
								"partsLeft":0,
								"valuesSent":[], //Not needed?
								
								"resultName": realResultName,
								
							}
						};
						
						docClient.put(params2, function(err, data) {
							if (err) {
								console.error(JSON.stringify(err, null, 2));
							} else {
								console.log("Reduce Job added.");
							}
						});
					}
									
				});
				
			}
		}
	}
	
};
	
setInterval(db.addReduceJobs,1000);


db.getSubmissionProgress = function (conf,callback) {
	
	var params = {
		TableName: "Jobs",
		//KeyConditionExpression: "begins_with (jid, :subID)",
		ProjectionExpression: "#s",//,jobType
		//IndexName: "priority-index",
		FilterExpression: "#s > :zero and begins_with (jid, :subID)",
		ExpressionAttributeNames:{
			"#s":"status"
		},
		ExpressionAttributeValues: {
			":zero": 0,
			":subID": conf.subID
		}
	 };
	 
	docClient.scan(params, onScan);

	var jobsDoneCount = 0;
	var jobsInProgressCount = 0;
	function onScan(err, data) {
		if (err) {
			console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
			if(callback) callback(undefined,err);
		} else {
			
			data["Items"].forEach(function(item) {
				if(item["status"] == 1){
					jobsInProgressCount++;
				} else if(item["status"] == 2) {
					jobsDoneCount++;
				} else {
					console.log("Weird bug getSubmissionProgress");
				}
			});
			
			if (typeof data.LastEvaluatedKey != "undefined") {
				params3.ExclusiveStartKey = data.LastEvaluatedKey;
				docClient.scan(params, onScan);
			} else {
				//console.log("jobsDoneCount="+jobsDoneCount +" conf.subID="+conf.subID+" submissionSizes[conf.subID]=" + (conf.subID ? submissionSizes[conf.subID] : "no conf.subID"));
				
				if(callback) callback({"done":jobsDoneCount,"inProgress":jobsInProgressCount,"total":submissionSizes[conf.subID]});
				
			}
			
		}
	}
	 
}

var calcReduceSize = function (size) {
	var total = 0;
	var half,leftOver;

	while(size > 1){
		half = Math.floor(size/2);
		total += half;
		leftOver = size - 2*half;
		size = half + leftOver;
		console.log("size = "+size);
	}

	return total;
}

db.throwReduceJobsIn = function(conf,callback){
	console.log("throwReduceJobsIn",conf);
	
	var resultName = [conf.subID,conf.resultName].join("=");
	
	//TODO save in config table
	resultFuncs[resultName] = conf.func;
	
	if(!(conf.subID in submissionSizes)) submissionSizes[conf.subID] = 0;
	submissionSizes[conf.subID] += calcReduceSize(conf.initSize);
	
	//TODO save in config table
	reduceJobResultNames.push(resultName);
	globalCheckedParts[resultName] = [];
	
	var reqs = [];
	
	for (var i = 0; i < conf.initSize; i+=2) {
		reqs.push({
			PutRequest: {
				Item: {
					
					"jobType":"reduceInit",
	
					"jid": [conf.subID,["reduce",conf.func,i].join("-")].join("="), //TODO Have a table to reach this. (Kind of encryptedish. Layer of security.)
	
					"priority":3* priorityLimit - (conf.priority + priorityLimit), // -priorityLimit when all parts ready.
					
					"status":0,
					"partsLeft":2, //TODO use as fallback, decrease priority directly as well.
					"valuesSent":[],
					
					"inputGroup":conf.inputGroup,
					"resultName": conf.resultName,
					"size":conf.initSize,
					//"parts":[i,i+1]
					
				}
			}
		});
		
		//TODO do this in parts if size is too big.
	}
	
	//TODO***** fix odd parts problem.
	
	var params = {
		RequestItems: {
			"Jobs": reqs
		}
	};

	docClient.batchWrite(params, function (err, data) {
		if (err)
			console.log(JSON.stringify(err, null, 2));
		else
			console.log(JSON.stringify(data, null, 2));
		
		//TODO check UnprocessedItems items in data.
		
		if(callback) callback();
		
	});
		
};

db.throwMapJobsIn = function(conf,callback){
	console.log("throwMapJobsIn",conf);
	
	if(!(conf.subID in submissionSizes)) submissionSizes[conf.subID] = 0;
	submissionSizes[conf.subID] += conf.size;
	
	var reqs = [];
	
	for (var i = 0; i < conf.size; i++) {
		reqs.push({
			PutRequest: {
				Item: {
					
					"jobType":"map",
	
					"jid": [conf.subID,conf.func + "-" + i].join("="), //TODO Have a table to reach this. (Kind of encryptedish. Layer of security.)
					
					"subID":conf.subID, //TODO use hash table for added layer of security.
	
					"priority":3* priorityLimit - (conf.priority + priorityLimit), // -priorityLimit when all parts ready.
					
					"status":0,
					"partsLeft":conf.inputGroups.length, //TODO use as fallback, decrease priority directly as well.
					"valuesSent":[],
					"reqBase":conf.reqBase,
					
					"inputGroups":conf.inputGroups,
					"outputGroups": conf.outputGroups,
					"size":conf.size
					
				}
			}
		});
		
		//TODO do this in parts if size is too big.
	}
	
	var params = {
		RequestItems: {
			"Jobs": reqs
		}
	};

	docClient.batchWrite(params, function (err, data) {
		if (err)
			console.log(JSON.stringify(err, null, 2));
		else
			console.log(JSON.stringify(data, null, 2));
		
		//TODO check UnprocessedItems items in data.
		
		if(callback) callback();
		
	});
	
};



db.throwDataPartsIn = function(conf,callback){
	console.log("throwDataPartsIn",conf);
	
	var reqs = [];
	
	for (var i = 0; i < conf.size; i++) {
		reqs.push({  
			PutRequest: {
				Item: {"group":[conf.subID,conf.group].join("="), "part":''+i, "usedAt":conf.usedAt || [], "status":0}
			}
		});
		
		//TODO do this in parts if size is too big.
	}
	
	var params = {
		RequestItems: {
			"Groups": reqs
		}
	};

	docClient.batchWrite(params, function (err, data) {
		if (err)
			console.log(JSON.stringify(err, null, 2));
		else{
			console.log(JSON.stringify(data, null, 2));
			
			//TODO save this and load back from Config Table
			GroupSizes[conf.group] = conf.size;
			
		}
		
		//TODO check UnprocessedItems items in data.
		
		if(callback) callback(err, data);
		
	});
	
};



db.getDataPart = function(conf,callback){
	console.log("getDataPart",conf);
	
	var subID = conf.subID;
	
	var params = {
		TableName: "Groups",
		Key: {
			"group": [subID,conf.group].join("="),
			"part": ''+conf.part //To be sure it is string.
		},
		ConsistentRead: true
	};

	docClient.get(params, function(err, data) {
		if (err) {
			callback(undefined,JSON.stringify(err, null, 2));
		} else {
			
			console.log("getDataPart " + JSON.stringify(data, null, 2));
			
			if("Item" in data){
				
				var item = data["Item"];
				if(!("status" in item)){
					callback(undefined,"Data not yet set:" + JSON.stringify(item));
				} else if (! item["status"] > 0) {
					callback(undefined,"Job not done yet:" + JSON.stringify(item));
				} else {
					callback(item["Val"]);
				}
				
			} else { //empty, not in db.
				callback(undefined,"Not in DB.");
			}
			
		}
	});
	
};

db.sendDataPart = function(conf,callback){
	console.log("sendDataPart",conf);
	
	var subID = conf.subID;
	
	var params = {
		TableName: "Groups",
		Key: {
			"group": [subID,conf.group].join("="),
			"part": ''+conf.part
		},
		UpdateExpression: "SET Val = :d, #s = :s",
		ExpressionAttributeNames:{
		    "#s":"status"
		},
		ExpressionAttributeValues: {
			":d": conf.data,
			":s":1
		},
		//ConditionExpression: "attribute_not_exists(Val) AND #s = :sold",
		ReturnValues: "ALL_NEW"
	};

	docClient.update(params, function(err, data) {
		
		if(callback) callback();
		
		if (err) {
			//if(callback) callback(undefined,JSON.stringify(err, null, 2));
			console.log("sendDataPart 1" +JSON.stringify(err, null, 2));
		} else {
			//if(callback) callback(JSON.stringify(data, null, 2));
			//console.log(JSON.stringify(data, null, 2));
			
			var usedAt = data["Attributes"]["usedAt"];
			
			var part = conf.part;
			
			usedAt.forEach(function(func) {
				
				var jid;
				
				if(func.indexOf("reduce-") == 0){
					
					var funcSplit = func.split("-");
					var funcName = funcSplit[1];
					var resultName = funcSplit[2];
					
					var partNum = parseInt(part);
					
					//TODO update this after changing jids.
					jid = "reduce-" + funcName + '-' + (partNum - (partNum % 2));
					
					
					//TODO update for more than 2 part reduce
					if( partNum == (GroupSizes[conf.group] - 1) && (GroupSizes[conf.group] %2) > 0 ){
						
						var params3 = {
							TableName: "Results",
							Item: {
								"resultName": [subID,resultName].join("="),
								"id": randomID(10),
								"Val": conf.data,
								"status":0
							},
							ConditionExpression: "attribute_not_exists(id)"
						};
						
						console.log("sendDataPart params3", JSON.stringify(params3,null,2));
						
						docClient.put(params3, function(err,data) {
							
							//TODO if same ID exists (very unlikely case, throw it again with different ID.)
							
							if (err)
								console.log("left over part "+JSON.stringify(err, null, 2));
								//TODO retry?
							else{
								console.log("left over part "+JSON.stringify(data, null, 2));
								
								////TODO remove after switching to smt better.
								//localOddEven++;
								
							}
						})
						
						return;
					}
				
					
				} else {
					//TODO update this after changing jids.
					jid = func + '-' + part;
				}
				
				var params2 = {
					TableName: "Jobs",
					Key: {
						"jid": [subID,jid].join("="),
					},
					UpdateExpression: "SET partsLeft = partsLeft - :one, priority = priority - :one", //Do we need -1 here?
					ExpressionAttributeValues: {
						":one": 1
					},
					ReturnValues: "ALL_NEW"
				};
				
				docClient.update(params2, function(err, data) {
					if (err)
						console.log("updatingJobs after sendData " +JSON.stringify(err, null, 2) + " jid:"+jid);
					else {
						
						if(data["Attributes"]["partsLeft"] == 0){
							
							var params3 = {
								TableName: "Jobs",
								Key: {
									"jid": [subID,jid].join("="),
								},
								UpdateExpression: "SET priority = priority + :benjamin",
								ExpressionAttributeValues: {
									":benjamin": priorityLimit
								},
								ReturnValues: "ALL_NEW"
							};
							
							docClient.update(params3, function(err, data) {
								if (err)
									console.log("updatingJobs after sendData " +JSON.stringify(err, null, 2) + " jid:"+jid);
								else
									console.log("updatingJobs after sendData  jid:"+jid);

							});
							
							
						}
						
					}
				});
					
				
			});
			
			
			//For keeping record.
			var params2 = {
				TableName: "Jobs",
				Key: {
					"jid":conf.jid
				},
				UpdateExpression: "SET valuesSent = list_append(valuesSent,:d)",
				ExpressionAttributeValues: {
					":d": [{"group":conf.group,"part":''+conf.part}]
				}
			};
			docClient.update(params2, function(err, data) {
				if (err)
					console.log("sendDataPart 2:" +JSON.stringify(err, null, 2));
				//else
					//console.log(JSON.stringify(data, null, 2));
			});
			
		}
			
	});
	
};

db.throwJobsBackToPool = function () {
	
	//(Date.now() - TimeGiven) > 30000
	//Date.now() - 30000 > TimeGiven
	
	var params = {
		TableName: "Jobs",
		IndexName: "priority-index",
		FilterExpression: "#s = :one AND TimeGiven < :tdmin",
		ProjectionExpression: "#s, TimeGiven, jid",
		ExpressionAttributeNames:{
			"#s":"status"
		},
		ExpressionAttributeValues: {
			":one": 1,
			":tdmin": (Date.now() - 30000)
		},
		//Limit: 5,//Limit: 1 //TODO put this after you make sure everything works correct.
	 };
	
	docClient.scan(params, function(err, data) {
		if (err) {
			//There are no jobs?
			console.log("throwJobsBackToPool err=",JSON.stringify(err, null, 2));
		} else {
			//console.log("throwJobsBackToPool data=",JSON.stringify(data, null, 2));
			
			if("Items" in data && data["Items"].length > 0){
				
				data["Items"].forEach(function(item) {
					
					var params = {
						TableName: "Jobs",
						Key: {
							"jid":item.jid,
						},
						UpdateExpression: "SET #s = :zero, TimeGiven = :zero",
						ConditionExpression: "#s = :one",
						ExpressionAttributeNames:{
							"#s":"status"
						},
						ExpressionAttributeValues: {
							":zero": 0,
							":one":1
						},
						ReturnValues: "NONE"
					};
					
					docClient.update(params, function(err, data) {
						if (err) {
							console.log("throwJobsBackToPool err="+JSON.stringify(err, null, 2) + " item=" + JSON.stringify(item, null, 2));
						} else {
							console.log("throwJobsBackToPool item=" + JSON.stringify(item, null, 2));
						}
					});
					
					
				});
				
			}
			
		}
	});
	
	
}

setInterval(db.throwJobsBackToPool,10000);


var getPriorityJobHelper = function (callback,lastKey){
	
	var params = {
		TableName: "Jobs",
		IndexName: "priority-index",
		FilterExpression: "partsLeft = :zero AND #s = :zero",
		ExpressionAttributeNames:{
			"#s":"status"
		},
		ExpressionAttributeValues: {
			":zero": 0
		},
		//Limit: 5,//Limit: 1 //TODO put this after you make sure everything works correct.
	 };
	 
	 if(lastKey) params.ExclusiveStartKey = lastKey;

	docClient.scan(params, function(err, data) {
		if (err) {
			//There are no jobs?
			callback(undefined,JSON.stringify(err, null, 2));
		} else {
			console.log(data["Count"],data["Count"],data["ScannedCount"],JSON.stringify(data["LastEvaluatedKey"], null, 2));
			if("Count" in data && data["Count"] > 0){
				
				//console.log("getPriorityJobHelper data['Items']", data["Items"]);
				
				var items = data["Items"];
				var randomItem = items[Math.floor(Math.random()*items.length)]; //TODO remove this if limit can be 1.
				var jid = randomItem.jid;
				
				var params = {
					TableName: "Jobs",
					Key: {
						"jid":jid,
					},
					UpdateExpression: "SET #s = :one, TimeGiven = :tg",
					ConditionExpression: "#s = :zero",
					ExpressionAttributeNames:{
						"#s":"status"
					},
					ExpressionAttributeValues: {
						":zero": 0,
						":one":1,
						":tg": Date.now()
					},
					ReturnValues: "ALL_NEW"
				};

				docClient.update(params, function(err, data) {
					if (err) {
						console.log("Expected fail "+JSON.stringify(err, null, 2));
						getPriorityJobHelper(callback);
					} else {
						//console.log(JSON.stringify(data, null, 2));
						callback(data["Attributes"]);
					}
				});
				
			} else if("ScannedCount" in data && data["ScannedCount"] > 0) {
				
				//TODO check if LastEvaluatedKey priority is > priorityLimit (meaning unused for now)
				
				if("LastEvaluatedKey" in data 
				   && data["LastEvaluatedKey"] != undefined
				   && data["LastEvaluatedKey"] != null ){
					
					//TODO only scan 1-2 times ?
					getPriorityJobHelper(callback,data["LastEvaluatedKey"]);
					
				} else {
					callback(undefined,"Coudln't find any jobs for now.");
				}
			} else {
				callback(undefined,"Coudln't find any jobs for now.");
			}
		}
	});
	
}

db.getPriorityJob = function(conf,callback){
	console.log("getPriorityJob",conf);
	
	//TODO
	
	getPriorityJobHelper(function (data,err) {
		
		if(err){
			callback(undefined,err);
		} else {
			if(data){
				
				var subIDnJid = data.jid.split("=");
				var subID = subIDnJid[0];
				var jid = subIDnJid[1];
				
				data["subID"] = subID;
				
				console.log("getPriorityJob data="+JSON.stringify(data,null,2));
				
				var funcNPart = jid.split("-");
				if(funcNPart[0] == "reduce"){
					data.func = funcNPart[1];
					//data.part = funcNPart[2];
					
					var partNum = parseInt(funcNPart[2]);
					
					data.parts = [partNum,partNum+1];
				} else if (funcNPart.length == 3){
					console.log("reduce job from result parts : "+ funcNPart[1] + " " +funcNPart[2]);
					data.func = funcNPart[0];
					data.parts = [funcNPart[1],funcNPart[2]];
				} else {
					data.func = funcNPart[0];
					data.part = funcNPart[1];
				}
				
				callback(data);
				
			} else {
				callback(undefined,"Weird error.");
			}
		}
		
	});
	
};

db.finishJob = function(conf,callback){
	console.log("finishJob",conf);
	
	//TODO send analytics info as well.
	
	var params = {
		TableName: "Jobs",
		Key: {
			"jid":conf.jid
		},
		UpdateExpression: "SET #s = :s, priority = priority - :p",
		ExpressionAttributeNames:{
		    "#s":"status"
		},
		//TODO check if s = 1 before.
		ExpressionAttributeValues: {
			":s": 2,
			":p":2*priorityLimit
		}
		//ReturnValues: "ALL_NEW"
	};

	docClient.update(params, function(err, data) {
		if (err){
			console.log("finishJob" +JSON.stringify(err, null, 2));
		} else {
			
			
			
		}
		
	});
	
};

db.getResultPart = function(conf,callback){
	console.log("db.getResultPart",conf);
	
	var resultName = [conf.subID,conf.resultName].join("=");
	var partID = conf.partID;
	
	var params = { 
		TableName: "Results",
		Key: {
			"resultName": resultName,
			"id": partID
		},
		ConsistentRead: true
	};
	console.log("db.getResultPart params",params);
	docClient.get(params, function(err, data) {
		if (!err && data && "Item" in data && "Val" in data["Item"]) {
			console.log("getResultPart data="+JSON.stringify(data, null, 2));
			callback(data["Item"]["Val"]);
		} else if(err) {
			console.log("getResultPart"+JSON.stringify(err, null, 2));
			callback(undefined,err);
		} else {
			console.log("Weird error getResultPart data="+JSON.stringify(data, null, 2));
			callback(undefined,"Weird error");
		}
	});

	
}

db.sendResultPart = function(conf,callback){
	console.log("sendResultPart",conf);
	
	var resultName = [conf.subID,conf.resultName].join("=");

	var params = {
		TableName: "Results",
		Item: {
			"resultName": resultName,
			"id": randomID(10),
			"Val": conf.data,
			"jid":conf.jid,
			"status":0
		},
		ConditionExpression: "attribute_not_exists(id)"
	};
	
	//console.log("sendResultPart params",JSON.stringify(params,null,2));
	
	docClient.put(params, function(err,data) {
		
		//TODO if same ID exists (very unlikely case, throw it again with different ID.)
		
		if (err)
			console.log("left over part "+JSON.stringify(err, null, 2));
		else{
			console.log("left over part "+JSON.stringify(data, null, 2));
			
			var jidSplit = conf.jid.split("-");
			var oldPart1 = jidSplit[1];
			var oldPart2 = jidSplit[2];
			
			var params1 = {
				TableName: "Results",
				Key: {
					"resultName": resultName,
					"id": oldPart1
				},
				UpdateExpression: "SET #s = :s",
				ExpressionAttributeNames:{
					"#s":"status"
				},
				ExpressionAttributeValues: {
					":s":1
				},
				ConditionExpression: "attribute_exists(id)"
				//TODO condition for status being 0
			}
			
			docClient.update(params1, function(err, data) {
				if (err) {
					//console.log("sendResultPart 1 Expected1" +JSON.stringify(err, null, 2));
				} else {
					
				}
			});
			
			var params2 = {
				TableName: "Results",
				Key: {
					"resultName": resultName,
					"id": oldPart2
				},
				UpdateExpression: "SET #s = :s",
				ExpressionAttributeNames:{
					"#s":"status"
				},
				ExpressionAttributeValues: {
					":s":1
				},
				ConditionExpression: "attribute_exists(id)"
				//TODO condition for status being 0
			}
			
			docClient.update(params2, function(err, data) {
				if (err) {
					//console.log("sendResultPart 1 Expected2" +JSON.stringify(err, null, 2));
				} else {
					
				}
			});
		}
	})
	
};


db.getFinalResult = function(conf,callback){
	console.log("getFinalResult",conf);
	
	var resultName = [conf.subID,conf.resultName].join("=");
	
	var params = {
		TableName: "Results",
		//IndexName: "GenreAndPriceIndex",
		KeyConditionExpression: "resultName = :rn",
		FilterExpression:"#s = :s",
		ExpressionAttributeNames:{
		    "#s":"status"
		},
		ExpressionAttributeValues: {
			":rn": resultName,
			":s":0
		},
		ConsistentRead: true
	};
	
	docClient.query(params, function(err, data) {
		if (err){
			console.log(JSON.stringify(err, null, 2));
			
			callback(undefined,"Error");
			
		} else {
		
			if("Count" in data && data["Count"] > 1 ||  data["Count"] == 0){
				callback(undefined,"Job not done yet.");
			} else {
				callback(data["Items"][0]);
			}
			
		}
	});
	
};

module.exports = db;
