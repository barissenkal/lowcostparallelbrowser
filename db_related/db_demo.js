// var path = require('path');
// delete require.cache[path.resolve('./example.js')];

var fs = require('fs');
var db = require('./dynamo.js');

db.throwJobsBackToPool();

return;

db.getPriorityJob({},function (data,err) {
	console.log(JSON.stringify(err, null, 2));
	console.log(JSON.stringify(data, null, 2));
})

var N = 3;

db.init({},function() {
	
	db.throwDataPartsIn({"group":"g1","usedAt":["func3"],"size":N*N});
	db.throwReaderJobsIn({"func":"func1","json":"node.json","group":"g1","priority":0,"size":N*N});

	db.throwDataPartsIn({"group":"g2","usedAt":["func3"],"size":N*N});
	db.throwReaderJobsIn({"func":"func2","json":"node.json","group":"g2","priority":0,"size":N*N});
	
	db.throwDataPartsIn({"group":"g3","usedAt":[],"size":N*N});
	
	db.throwMapJobsIn({"func":"func3","size":N*N,"inputGroups":["g1","g2"],"outputGroups":["g3"],"priority":3});
	
	db.throwReduceJobsIn({"func":"func4","initSize":N*N,"inputGroup":"g3","resultName":"result1","priority":5});
	
	db.getDataPart({"group":"g1","part":0},function(data,err) {
		//TODO error check
		console.log(data,err);
	});
	
	db.sendDataPart({"group":"g1","part":0,"data":{"demo":"demo0"},"jid":"func1-0"},function(data,err) {
		//TODO error check
		console.log(data,err);
	})
		
	db.sendDataPart({"group":"g2","part":0,"data":{"demo":"demo0"},"jid":"func1-0"},function(data,err) {
		//TODO error check
		console.log(data,err);
	})
	
	setTimeout(function() {
		
		db.finishJob({"jid":"func1-0"},function(data,err) {
			//TODO error check
			console.log(data,err);
		})
		
	}, 1000);
	
	setTimeout(function() {
		
		db.getDataPart({"group":"g1","part":0},function(data,err) {
			//TODO error check
			console.log(data,err);
		});
		
	}, 2000);
	
	
	
});