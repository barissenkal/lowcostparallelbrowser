


//delete or not
var dlt = false || process.env.DLT;




var fs = require('fs');
var path = require('path');

var debug = require('debug')('lcccmb:db');

var AWS = require("aws-sdk");

//TODO switch from local DynamoDB to Amazon hosted with "DBURL" enviroment var.

AWS.config.update({
  region: "eu-west-1",
  endpoint: "https://dynamodb.eu-west-1.amazonaws.com"
});

var docClient = new AWS.DynamoDB.DocumentClient();

function scanWholeTable(tableName,IndexName) {
	var params = {
		TableName: tableName,
	};
	
	if(IndexName){
		params.IndexName = IndexName;
		//params.ScanIndexForward = false;
	}
	
	var str = "";

	docClient.scan(params, onScan);

	function onScan(err, data) {
		if (err) {
			console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
		} else {
			// print all the movies
			console.log("Scan succeeded.");
			data.Items.forEach(function(item) {
				//console.log(JSON.stringify(item, null, 2));
				str += "," + JSON.stringify(item, null, 2);
			});

			// continue scanning if we have more movies, because
			// scan can retrieve a maximum of 1MB of data
			if (typeof data.LastEvaluatedKey != "undefined") {
				console.log("Scanning for more...");
				params.ExclusiveStartKey = data.LastEvaluatedKey;
				docClient.scan(params, onScan);
			} else {
				fs.writeFileSync(path.join(__dirname, tableName+'.json'),str);
			}
		}
	}
}

function deleteResultsTable() {
	var params = {
		TableName: "Results",
		ProjectionExpression: "resultName,id",
	};

	var str = "";

	docClient.scan(params, onScan);

	function onScan(err, data) {
		if (err) {
			console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
		} else {
			
			data.Items.forEach(function(item) {
				
				var params2 = {
				    TableName: "Results",
				    Key: {
				        "resultName": item["resultName"],
				        "id": item["id"],
				    }
				};

				docClient.delete(params2, function(err, data) {
				    if (err)
				        console.log(JSON.stringify(err, null, 2));
				    else
				        console.log(JSON.stringify(data, null, 2));
				});
				
			});

			// continue scanning if we have more movies, because
			// scan can retrieve a maximum of 1MB of data
			if (typeof data.LastEvaluatedKey != "undefined") {
				params.ExclusiveStartKey = data.LastEvaluatedKey;
				docClient.scan(params, onScan);
			}
		}
	}

}
	
			
function deleteJobsTable() {
			
			var params = {
				TableName: "Jobs",
				ProjectionExpression: "jid",
			};

			var str = "";

			docClient.scan(params, onScan2);

			function onScan2(err, data) {
				if (err) {
					console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
				} else {
					data.Items.forEach(function(item) {
						
						var params2 = {
						    TableName: "Jobs",
						    Key: {
						        "jid": item["jid"]
						    }
						};

						docClient.delete(params2, function(err, data) {
						    if (err)
						        console.log(JSON.stringify(err, null, 2));
						    //else
						        //console.log(JSON.stringify(data, null, 2));
						});
						
					});

					// continue scanning if we have more movies, because
					// scan can retrieve a maximum of 1MB of data
					if (typeof data.LastEvaluatedKey != "undefined") {
						params.ExclusiveStartKey = data.LastEvaluatedKey;
						docClient.scan(params, onScan2);
					}
					
				}
			}	
		}		
			

function deleteGroupsTable() {
	
			var params = {
				TableName: "Groups",
				ProjectionExpression: "#g,part",
				ExpressionAttributeNames:{
					"#g":"group"
				},
			};

			var str = "";

			docClient.scan(params, onScan3);

			function onScan3(err, data) {
				if (err) {
					console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
				} else {
					data.Items.forEach(function(item) {
						
						var params2 = {
						    TableName: "Groups",
						    Key: {
						        "group": item["group"],
						        "part": item["part"],
						    }
						};

						docClient.delete(params2, function(err, data) {
						    if (err)
						        console.log(JSON.stringify(err, null, 2));
						    //else
						        //console.log(JSON.stringify(data, null, 2));
						});
						
					});

					// continue scanning if we have more movies, because
					// scan can retrieve a maximum of 1MB of data
					if (typeof data.LastEvaluatedKey != "undefined") {
						params.ExclusiveStartKey = data.LastEvaluatedKey;
						docClient.scan(params, onScan3);
					}
					
				}
			}
}

if(dlt){
	console.log("Deleting");
	deleteResultsTable();
	deleteJobsTable();
	deleteGroupsTable();
} else {
	console.log("Saving");
	scanWholeTable("Results");
	scanWholeTable("Jobs","priority-index");
	scanWholeTable("Groups");
}


