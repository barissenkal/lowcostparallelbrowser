var debug = require('debug')('lcccmb:db');

var fakeDB;
var db = {};

db.name = "example";

function jID() {
	
	//gnu id generator
	return 'xxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx'.replace(/[xy]/g, function (c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}

db.init = function(conf) {
	fakeDB = {
		"jobs":[],
		//"finishedJobs":[],
		"jobSizes":{},
		"groups":{},
		"unfinishedJobData":[],
		"resultGroups":{},
		"oddParts":[],
		"resultFuncs":{}
	};
	db.fakeDB = fakeDB;
	
	//TODO timeouts for functions to handle errors better.

}

db.throwReduceJobsIn = function(conf,callback){
	console.log("throwReduceJobsIn",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	var size = conf.initSize;
	
	fakeDB["resultFuncs"][conf.resultName] = conf.func;
	
	for (var i = 0; i < size -1; i+=2) {
		
		fakeDB.jobs.push({
			"jobType":"reduceInit",
			"func":conf.func,
			"inputGroup":conf.inputGroup,
			"resultName": conf.resultName,
			"parts":[i,i+1],
			"partsLeft":2,
			"priority":conf.priority,
			"status":0,
			"jid":jID()
		});
		
		fakeDB["groups"][conf.inputGroup][i]["usedAt"].push(conf.func);
		fakeDB["groups"][conf.inputGroup][i+1]["usedAt"].push(conf.func);
		
	}
	
	if(size % 2){
		//TODO queue for background job instead ?
		fakeDB["oddParts"].push({"resultName":conf.resultName,"part":size - 1,"group":conf.inputGroup});
	}
		
};
db.throwMapJobsIn = function(conf,callback){
	console.log("throwMapJobsIn",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	var size = conf.size;
	
	fakeDB["jobSizes"][conf.func] = size; //TODO use id instead
	
	for (var i = 0; i < size; i++) {
		for (var j = 0; j < conf.inputGroups.length; j++) {
			//TODO use IDs instead. TODO don't use func names id in frontend.
			fakeDB["groups"][conf.inputGroups[j]][i]["usedAt"].push(conf.func);
		}
		
		fakeDB.jobs.push({
			"jobType":"map",
			"func":conf.func,
			"inputGroups":conf.inputGroups,
			"partsLeft":conf.inputGroups.length,
			"outputGroups": conf.outputGroups,
			"part":i,
			"priority":0,
			"status":0,
			"jid":jID()
		});
	}
	
};
db.throwReaderJobsIn = function(conf,callback){
	console.log("throwReaderJobsIn",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	var group = fakeDB["groups"][conf.group];
	
	var size = group.length;
	
	fakeDB["jobSizes"][conf.func] = size; //TODO use id instead
	
	for (var i = 0; i < size; i++) {
		//TODO priority comes from conf.
		fakeDB.jobs.push({
			"jobType":"read",
			"json":conf.json,
			"group":conf.group,
			"func":conf.func,
			"part":i,
			"priority":0,
			"status":0,
			"jid":jID()
		});
	}
	
};

db.throwDataPartsIn = function(conf,callback){
	console.log("throwDataPartsIn",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	var group = fakeDB["groups"][conf.group] = [];
	
	for (var i = 0; i< conf.size; i++) {
		group.push({"part":i,"usedAt":[],"status":0});
	}
	
	if(callback) callback();
	
};



db.getDataPart = function(conf,callback){
	console.log("getDataPart",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	if (!(conf.group in fakeDB["groups"])) {
		callback(undefined,"Unknown Group Name");
		return;
	}
	
	var group = fakeDB["groups"][conf.group];
	
	for (var i = 0; i < group.length; i++) {
		var item = group[i];
		if(item.part == conf.part){
			
			if("data" in item){
				callback(JSON.parse(JSON.stringify(item["data"])));
			} else {
				callback(undefined,"Data not yet set:" + JSON.stringify(item));
			}
			
			return;
		}
	}
	
	callback(undefined,"Non existant part number");
	
};

db.sendDataPart = function(conf,callback){
	console.log("sendDataPart",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	//TODO check conf before hand.
	//TODO check if part and group is valid.
	
	fakeDB["unfinishedJobData"].push({
		"data":conf.data,
		"group":conf.group,
		"part":conf.part,
		"jid":conf.jid
	});
	
};


db.getPriorityJob = function(conf,callback){
	console.log("getPriorityJob",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	//In real db, use parts left then priority to short
	
	var highestPriorityJob;
	
	for (var i = 0; i < fakeDB["jobs"].length; i++) {
		if(fakeDB["jobs"][i].partsLeft > 0 || fakeDB["jobs"][i].status != 0) continue;
		var job = fakeDB["jobs"][i];
		if (!highestPriorityJob || job.priority < highestPriorityJob.priority) {
			highestPriorityJob = fakeDB["jobs"][i];
			if(highestPriorityJob.priority == 0) break; //To keep start faster
		}
	}
	
	if(highestPriorityJob){
		highestPriorityJob.status = 1;
		highestPriorityJob.size = fakeDB["jobSizes"][highestPriorityJob.func]; //TODO use id instead.
		callback(JSON.parse(JSON.stringify(highestPriorityJob)));
	} else {
		callback(undefined,"No avaliable job found");
	}
	
};

db.finishJob = function(conf,callback){
	console.log("finishJob",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	var group = fakeDB["groups"][conf.group];
	
	for (var i = fakeDB["unfinishedJobData"].length - 1; i >= 0; i--) {
		
		if(fakeDB["unfinishedJobData"][i] && fakeDB["unfinishedJobData"][i].jid != conf.jid) continue;
		
		var finishedPart = fakeDB["unfinishedJobData"][i];
		
		if(finishedPart.group in fakeDB["groups"]){
			var group = fakeDB["groups"][finishedPart.group];
			
			for (var j = 0; j < group.length; j++) {
				var item = group[j];
				if(item.part == finishedPart.part){
					
					item.data = finishedPart.data;
					
					//TODO update usedAt's partsLefts
					
					var jobNames = item.usedAt;
					
					fakeDB["jobs"].map(function (job) {
						if ("resultName" in job && finishedPart.group == job.inputGroup) {
							
							for (var z = job.parts.length - 1; z >= 0; z--) {
								if(job.parts[z] == item.part) job.partsLeft--;
							}
							
						} else if (job.part == finishedPart.part && jobNames.indexOf(job.func) > -1) {
							job.partsLeft--;
						}
					});
					
					break;
					
				}
			}
			
			for (var k = 0; k < fakeDB["oddParts"].length; k++) {
				if(fakeDB["oddParts"][k].part != finishedPart.part || fakeDB["oddParts"][k].group != finishedPart.group) continue;
				
				var resultName = fakeDB["oddParts"][k].resultName;
				
				if(!(resultName in fakeDB["resultGroups"])) fakeDB["resultGroups"][resultName] = [];
						
				fakeDB["resultGroups"][resultName].push({"data":finishedPart.data,"status":0});
			}
			
			//fakeDB["unfinishedJobData"].splice(i,1);
			
		} else {
			//TODO error.
		}
	}
	
	// for (var i = 0; i < fakeDB["jobs"].length; i++) {
	// 	if(fakeDB["jobs"][i].status != 1) continue;
	// 	if(fakeDB["jobs"][i].jid != conf.jid) continue;
		
	// 	var job = fakeDB["jobs"][i];
	// 	job.status = 2;
	// 	fakeDB["finishedJobs"].push(job);
		
	// 	console.log("job"+JSON.stringify(job));
		
	// 	//fakeDB["unfinishedJobData"].splice(i,1);
	// 	break;
	// }
	
};



db.getResultParts = function(conf,callback){
	console.log("getResultParts",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	var resultName = conf.resultName;
	
	if(!(resultName in fakeDB["resultGroups"])) {
		callback(undefined,"No parts with given resultName");
		return;
	}
	
	//TODO:Save jid in part (when setting status 1)
	
	//TODO do this with ... in db
	var parts = [];
	var group = fakeDB["resultGroups"][resultName];
	
	for (var i = 0; i< group.length; i++) {
		if(parts.length == 2) break;
		if(group[i].status != 0) continue;
		
		parts.push(group[i]);
	}
	
	if(parts.length == 2){
		
		parts[0].status = 1;
		parts[0].jid = conf.jid;		
		parts[1].status = 1;
		parts[1].jid = conf.jid;
		
		var datas = [parts[0].data,parts[1].data];
		
		callback(JSON.parse(JSON.stringify(datas)));
	} else {
		callback(undefined,"Not enough parts found");
	}
	
};

db.sendResultPart = function(conf,callback){
	console.log("sendResultPart",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	var resultName = conf.resultName;
	var jid = conf.jid;
	
	if(!(resultName in fakeDB["resultGroups"])) {
		fakeDB["resultGroups"][resultName] = [];
	}
	
	var group = fakeDB["resultGroups"][resultName];
	
	console.log("the conf",conf);
	
	// status 0 = untouched, 1 = in progress
	group.unshift({
		"data": conf.data,
		"status": 0
	});
	
	//remove parts with same jid from resultGroups. (loop in reverse)
	for (var i = group.length - 1; i >= 0; i--) {
		if(!group[i]) continue;
		if(!("jid" in group[i])) continue;
		if(group[i].jid == jid) group.splice(i,1); //group[i] = undefined; 
	}
	
	//This function should add one reduce job per 2 new reduce parts
	//Do this with reduce part count? if # of parts / 2 < # of jobs, add job.
	
	var numOfJobs = 0;
	for (var i = fakeDB["jobs"].length - 1; i >= 0; i--) {
		if(fakeDB["jobs"][i].jobType == "reduce" && fakeDB["jobs"][i].status == 0) numOfJobs++;
	}
	var numOfParts = 0;
	for (var i = fakeDB["resultGroups"][resultName].length - 1; i >= 0; i--) {
		if(fakeDB["resultGroups"][resultName][i] && fakeDB["resultGroups"][resultName][i].status == 0) numOfParts++;
	}
	
	console.log("numOfParts,numOfJobs",numOfParts,numOfJobs);
	
	if(Math.floor(numOfParts/2) - numOfJobs >= 0){
		fakeDB.jobs.push({
			"jobType":"reduce",
			"resultName": conf.resultName,
			"func": fakeDB["resultFuncs"][conf.resultName],
			"status":0,
			"jid":jID()
		});
	}
	
};


db.getFinalResult = function(conf,callback){
	console.log("getFinalResult",conf);
	
	conf = JSON.parse(JSON.stringify(conf));
	
	//When there is a single item left in result group's array AND there are no reduce jobs left.

	var resultName = conf.resultName;
	
	if(resultName in fakeDB["resultGroups"] && fakeDB["resultGroups"][resultName].length == 1){
		callback(JSON.parse(JSON.stringify(fakeDB["resultGroups"][resultName][0])));
	} else {
		callback(undefined,"Didn't finish yet");
	}

};

module.exports = db;
