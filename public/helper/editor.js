			/* Node types:
			 * group = 0
			 * single = 1
			 * range = 2
			 * map = 3
			 * reduce = 4
			 * result = 5
			 */
			 
			var job = {
				"jobDescription":null,
				"funcs":{},
				"JSONs":{}
			};

			var autorun = (typeof(Storage) !== "undefined" && "autorun" in localStorage ? (localStorage.autorun == "true") : true);
			if(autorun){
				document.getElementById("autoUpdateCheckBox").checked = true;
			}
			var setAutorun = function (checked) {
				console.log(checked);
				if(checked) {
					localStorage.autorun = "true";
				} else {
					localStorage.autorun = "false";
				}
				autorun = checked;
			};

			var showNodeData = function(node){
				console.log(node);
				//TODO fix object attributes with human readable ones.
				
				var doNotShow = ["x","y","type","index","vy","vx","fx","fy","json"];
				
				var temp = "";
				for(var k in node){
					if(doNotShow.indexOf(k) > -1) continue;
					if(k == "id"){
						temp += '<h4 style="margin-bottom:0;">' + node[k];
						switch(node["type"]){
							case 0:
								if("json" in node){ //sourced case.
									var url = node["json"]
									var lastslash = url.lastIndexOf("/");
									var dir, file;
									if(lastslash > 0){
										dir = url.substring(0,lastslash+1);
										file = url.substring(lastslash+1);
									} else {
										dir = url;
									}
									
									temp += ' ("'+node["reader"]+'" reads from "<span style="text-overflow:ellipsis; max-width:20px;">'+dir+'</span>'+file+'")';
								} else {
									temp += " (Group)";
								}
								break;
							case 1:
								temp += " (Single)";
								break;
							case 2:
								temp += " (Range)";
								break;
							case 3:
								temp += " (Map)";
								break;
							case 4:
								temp += " (Reduce)";
								break;
						}
						temp += '</h4><br>';
					} else {
						temp += ['<b>',k,'</b> : ',node[k],' '].join('');
					}
				}
				document.getElementById("nodeDetails").innerHTML = temp;
			}

			var clearGraph = function() {
				
				//Clear node details
				document.getElementById("nodeDetails").innerHTML = '';
				
				lastStringified = '';
				
				var graphSVG = document.getElementById("graph");
				while (graphSVG.firstChild) {
					graphSVG.removeChild(graphSVG.firstChild);
				}
			}

			var lastStringified;
			var updateGraph = function(graph){
				
				//To keep updates from happening constantly
				var stringified = JSON.stringify(graph);
				if (lastStringified == stringified){
					document.getElementById("nodeDetails").innerHTML = '(Code resulted in same graph)';
					return;
				}
				
				lastStringified = stringified;
				
				var temp = JSON.parse(stringified);
				
				clearGraph();
				
				if(!("nodes" in temp) || !("links" in temp) || !("results" in temp) || temp.nodes.length <= 0){
					document.getElementById("nodeDetails").innerHTML = '<b>Nothing To Show</b><br>Re-check your code';
					return;
				}

				var svg = d3.select("#graph"),
					width = +svg.attr("width"),
					height = +svg.attr("height");
					
				var index = 0;
				for(var k in temp.nodes){
					if(temp.results.indexOf(temp.nodes[k].id) < 0) continue;
					
					temp.nodes[k].fy = height - 40;
					temp.nodes[k].fx = width/(temp.results.length + 1)* ++index;
				}

				var color = (function(){
					var colorCodes = ["#3288bd","#66c2a5","#abdda4","#e6f598", /*"#ffffbf",*/ "#fee08b",/*"#fdae61"*/"#f46d43","#d53e4f","#9e0142","#5e4fa2"];
					return function(i){
						return colorCodes[i % (colorCodes.length)];
					}
				})();
					
				var simulation = d3.forceSimulation()
					.force("link", d3.forceLink().distance(80).id(function(d) { return d.id; }))
					.force("charge", d3.forceManyBody())
					.force("center", d3.forceCenter(width / 2, height / 2));
					
				var circleRadius = 5 + Math.abs(15 - 2*Math.log(graph.nodes.length));
				if (circleRadius > 20) circleRadius = 20;
					
				svg.append("svg:defs").selectAll("marker")
				    .data(["end"])      // Different link/path types can be defined here
				  .enter().append("svg:marker")    // This section adds in the arrows
				    .attr("id", String)
				    .attr("viewBox", "0 -5 10 10")
				    .attr("refX", 16)
				    .attr("refY", 0)
				    .attr("markerWidth", circleRadius/4)
				    .attr("markerHeight", circleRadius/4)
				    .attr("orient", "auto")
				  .append("svg:path")
				    .attr("d", "M0,-5L10,0L0,5");

				var link = svg.append("g")
					.selectAll("line")
					.data(temp.links)
					.enter().append("line")
					.attr("class", "links")
					.attr("marker-end", "url(#end)");
					//.attr("stroke-width", function(d) { return 2;});//Math.sqrt(d.value); });

				var node = svg.append("g")
					//.attr("class", "nodes")
					.selectAll("circle")
					.data(temp.nodes)
					.enter().append("circle")
					.attr("r", circleRadius)
					.attr("fill", function(d) {return color(d.type);})
					.attr("class", function (d) {
						if (d && "type" in d){
							if (d.type >= 3 && d.type != 5) {
								return "task-node";
							} else {
								return "data-node";
							}	
						} else {
							return "false-node";
						}
					})
					.on('click' , showNodeData)
					.call(d3.drag()
						.on("start", dragstarted)
						.on("drag", dragged)
						.on("end", dragended));

				node.append("title")
					.text(function(d) { return d.id; });
				
				simulation
					.nodes(temp.nodes)
					.on("tick", ticked);

				simulation.force("link")
					.links(temp.links);
				
				function ticked() {
					node
						.attr("cx", function(d) { return d.x; })
						.attr("cy", function(d) { return d.y; })
						.attr("transform", function(d) {
							return "";
						});
					
					link
						.attr("x1", function(d) { return d.source.x; })
						.attr("y1", function(d) { return d.source.y; })
						.attr("x2", function(d) { return d.target.x; })
						.attr("y2", function(d) { return d.target.y; });

				}
				
				function dragstarted(d) {
					if(d.type == 5) return;
					
					if (!d3.event.active) simulation.alphaTarget(0.3).restart();
					d.fx = d.x;
					d.fy = d.y;
				}

				function dragged(d) {
					if(d.type == 5) return;
					d.fx = d3.event.x;
					d.fy = d3.event.y;
				}

				function dragended(d) {
					if(d.type == 5) return;
					if (!d3.event.active) simulation.alphaTarget(0);
					d.fx = null;
					d.fy = null;
				}

			}

			//So that page doesn't look empty.
			updateGraph({"nodes": [],"links": [],"results":[]});

			var updateStructure = (function(){
				
				var funcs = [
					function deneme(x){ //TODO delete this
						console.log(x);
					},
					function group(name,conf){
						//TODO check conf first
						
						//TODO consider other configurations with reader etc.
						
						graph.nodes.forEach(function(item){
							if(item.id == name){ throw "<b>[GROUP]:</b> name '"+name+"' already exist.";}
						});
						
						var tempNode = {"id":name,"type":0};
						for(var k in conf){
							//TODO check config first, throw error if not valid
							tempNode[k] = conf[k];
						}
						graph.nodes.push(tempNode);
						
						//TODO update Job Description as well
						
					},
					function single(name,conf){
						//TODO check conf first
						
						graph.nodes.forEach(function(item){
							if(item.id == name){ throw "<b>[SINGLE]:</b> name '"+name+"' already exist.";}
						});
						
						var tempNode = {"id":name,"type":1};
						for(var k in conf){
							//TODO check config first, throw error if not valid
							tempNode[k] = conf[k];
						}
						graph.nodes.push(tempNode);
						
						//TODO update Job Description as well
						
						// if(conf.value || conf.json) {
						// 	graph.imported.push(tempNode);
						// }
						
					},
					function range(name,conf){
						//TODO check conf first
						
						graph.nodes.forEach(function(item){
							if(item.id == name){ throw "<b>[RANGE]:</b> name '"+name+"' already exist.";}
						});
						
						var tempNode = {"id":name,"type":2};
						for(var k in conf){
							//TODO check config first, throw error if not valid
							tempNode[k] = conf[k];
						}
						graph.nodes.push(tempNode);
						
						//TODO update Job Description as well
					},
					function map(conf){
						//TODO check conf first
						
						var funcName = conf.func;
						var inputs = conf.inputs; //TODO make array if single node
						var outputs = conf.outputs; //TODO make array if single node
						
						var tempNode = {"id":funcName,"type":3,"inputs":inputs,"outputs":outputs};
						
						var inputsFound = 0;
						graph.nodes.forEach(function(item){
							if(item.type < 3 && inputs.indexOf(item.id) > -1){
								inputsFound++;
							}
						});
						
						if(inputs.length != inputsFound) throw "<b>[MAP]:</b> One or more inputs doesn't exist.";
						
						var outputsFound = 0;
						graph.nodes.forEach(function(item){
							if(item.type < 3 && outputs.indexOf(item.id) > -1){
								outputsFound++;
							}
						});
						
						if(outputs.length != outputsFound) throw "<b>[MAP]:</b> One or more outputs doesn't exist.";
						
						graph.nodes.push(tempNode);
						
						for(var k in inputs){
							graph.links.push({"source":inputs[k],"target":funcName});
						}
						for(var k in outputs){
							graph.links.push({"source":funcName,"target":outputs[k]});
						}
						
						//TODO update Job Description as well
					},
					function reduce(conf){
						//TODO check conf first
						
						var funcName = conf.func;
						var input = conf.input;
						//var output = conf.output;
						//var times = conf.times;
						var result = conf.result;
						
						//TODO consider other kinds of config.
						
						
						if(graph.results.indexOf(result) > -1) throw '<b>[REDUCE]:</b> result with name "'+result+'" already exists.';
						
						var inputFound = false;
						graph.nodes.forEach(function(item){
							
							if(item.type == 4 && input == item.input && funcName == item.id){
								throw '<b>[REDUCE]:</b> another reduce with input "'+input+'" and function "'+funcName+'" already exists.';
							} else if(item.type < 3 && input == item.id){
								inputFound = true;
							}
							
						});
						if(!inputFound) throw "<b>[REDUCE]:</b> input doesn't exist.";
						
						
						graph.nodes.push({"id":funcName,"type":4,"input":input,"result":result});
						graph.nodes.push({"id":result,"type":5});
						
						graph.links.push({"source":input,"target":funcName});
						graph.links.push({"source":funcName,"target":result});
						
						//results array helps organize the visual graph.
						graph.results.push(result);
						
					},
				]
				
				var funcText = funcs.map(function(func){
					return func.toString();
				}).join('\n');
				
				var preText = "var graph = {'nodes':[],'links':[],'results':[]};";

				var validateGraphAndDownloadLink = function(obj){
					
					console.log(obj);
					
					var runJobButton = document.getElementById("runJobButton");
					runJobButton.className = "btn btn-success nonclickable disabled";
					
					runJobButton.className = "btn btn-success";
					
					var temp = false;
					
					var hasCycle = function(path,current){
						obj.links.forEach(function(item){
							
							if(temp) return;
							
							if(item.target == current){
								
								console.log(path,item);
															
								if(path.indexOf(item.source) > -1) {
									console.log("wtf");
									temp = true;
									return;
								}
								var temp1 = hasCycle(path + "\n" + current,item.source);
								if (temp1){
									temp = true;
									return;
								}
							}
							
						});
					}
					
					var valid = true;
					for(var k in obj.results){
						if(hasCycle("",obj.results[k])){
							valid = false;
							break;
						}
					}
					
					if(valid){
						job["jobDescription"] = obj;
					} else {
						document.getElementById("nodeDetails").innerHTML = "<b>Job Description has a cycle in graph</b><br>Check if you used a group for it's creation.";
					}
				}

				return function (text) {

					//TODO Do we need to check for "for" "while" loops etc in text ?
					//TODO Do we need to check for "if" "else" in text ?
					
					
					try {
						var result = eval("(function(){" + preText + funcText + "\n" + text + " return graph;})();");
						
						console.log(result);
						
						//TODO update graph from result.
						
						updateGraph(result);
						
						validateGraphAndDownloadLink(result);
						
					} catch (error){
						console.log(error);
						
						clearGraph();
						
						document.getElementById("nodeDetails").innerHTML = error;
						
					}	
				}
			})();

			var myCodeMirror = CodeMirror(document.getElementById("editor"), {
				value: (typeof(Storage) !== "undefined" && "savedcode" in localStorage && !(localStorage.savedcode == "\n" || localStorage.savedcode == "" || localStorage.savedcode == "/* Hello, World! */\n\n") ? localStorage.savedcode :"var N = 3;\n\ngroup(\"g1\",{\n	\"reader\":\"func1\",\n	\"json\":\"m1.json\",\n	\"size\": N*N\n})\n\ngroup(\"g2\",{\n	\"reader\":\"func2\",\n	\"json\":\"m2.json\",\n	\"size\": N*N\n})\n\ngroup(\"g3\",{\n	\"ordered\":true,\n	\"size\":N*N\n})\n\nmap({\n	\"inputs\":[\"g1\",\"g2\"],\n	\"func\":\"func3\",\n	\"outputs\":[\"g3\"]\n})\n\nreduce({\n	\"input\":\"g3\",\n	\"func\":\"func4\",\n	\"result\":\"result1\"\n})\n\n"),
				mode: "javascript",
				theme: "atom-one-light",
				lineNumbers: true,
				lineWrapping: true,
				autoCloseBrackets:true
			});

			function updateFromCodeMirror(){
				var temp = "";
					myCodeMirror.getDoc().eachLine(function(line){
						temp += line.text + "\n";
					});
					
					//To remove last new line
					localStorage.savedcode = temp.substring(0,temp.length-1);
					
					updateStructure(temp);
			}

			var timeout1;
			myCodeMirror.on("changes",function(cm,obj){
				if(timeout1){
					clearTimeout(timeout1);
				}
				
				timeout1 = setTimeout(function() {
					if(autorun) updateFromCodeMirror();
				},500);
			});

			if(autorun) updateFromCodeMirror();