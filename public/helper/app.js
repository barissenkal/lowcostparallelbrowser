'use strict';

/* Node types:
 * group = 0
 * single = 1
 * range = 2
 * map = 3
 * reduce = 4
 * result = 5
 */

// Declare app level module which depends on views, and components
var app = angular.module('main', [
	 'ui.bootstrap',
	 'ngStorage',
	 'ngNotify',
	 'ui.ace',
	 'ngSanitize',
	 ]).
config(['$sceProvider', function($sceProvider) {
	$sceProvider.enabled(false);
}]);

//services

app.service('storageService', ['$rootScope','$timeout', '$location', function($rootScope,$timeout,$location) {
	var storageService = $rootScope.$new(true);
	
	storageService.apiAddress = ($location.host() == "localhost" ? "http://localhost:5000/api/" : "http://compujs.com/api/");
	
	storageService.jobDescriptionCode = (typeof(Storage) !== "undefined" && "savedcode" in localStorage && !(localStorage.savedcode == "\n" || localStorage.savedcode == "") ? localStorage.savedcode :"/* Check out blue examples button on top left  of the page */\n\n");
	
	storageService.saveJobDescriptionCode = function (jobDescriptionCode) {
		storageService.jobDescriptionCode = jobDescriptionCode;
		localStorage.savedcode = jobDescriptionCode;
	}
	
	storageService.autorun = (typeof(Storage) !== "undefined" && "autorun" in localStorage ? (localStorage.autorun == "true") : true);
	
	storageService.saveAutorun = function (autorun) {
		storageService.autorun = autorun;
		localStorage.autorun = autorun;
	}
	
	var savedFuncAndJSONs = (typeof(Storage) !== "savedfnj" && "savedfnj" in localStorage ? JSON.parse(localStorage.savedfnj) : { "funcs":{}, "JSONs":{} });
	
	storageService.saveFuncAndJSONs = function () {
		localStorage.savedfnj = JSON.stringify(savedFuncAndJSONs);
	}
	storageService.loadFuncNJSONs = function (obj) {
		for (var k in obj["funcs"]) {
			savedFuncAndJSONs["funcs"][k] = obj["funcs"][k];
		}
		for (var k in obj["JSONs"]) {
			savedFuncAndJSONs["JSONs"][k] = obj["JSONs"][k];
		}
		//console.log(savedFuncAndJSONs);
		storageService.saveFuncAndJSONs();
	}

	storageService.tabs = [];
	var tabIDs = [];
	storageService.clearTabs = function () {
		storageService.tabs = [];
		tabIDs = [];
	}
	var saveTabHelperTimeout;
	var saveTabHelper = function (kind,name,x) {
		
		if(kind == "json"){
			savedFuncAndJSONs["JSONs"][name] = x;
		} else {
			savedFuncAndJSONs["funcs"][name] = x;
		}
		
		$timeout.cancel(saveTabHelperTimeout);
		saveTabHelperTimeout = $timeout(function () {
			storageService.saveFuncAndJSONs();
		},500);
	}
	storageService.addTab = function (kind,name) {
		
		//Hotfix for urls
		if(kind == "json" && name.indexOf("http") == 0) return;
		
		//TODO check if exists before.
		if(tabIDs.indexOf(kind+"-"+name) > -1) return;
		
		tabIDs.push(kind+"-"+name);
		
		var timeout;
		storageService.tabs.push({
			id: kind+"-"+name,
			title: name,
			mode: (kind == "json" ? "json" :  "javascript"),
			explText: storageService.explanationFor(kind),
			code: storageService.codeFor(kind,name),
			onChange : function (e) {
				$timeout.cancel(timeout);
				timeout = $timeout(function() {
					saveTabHelper(kind,name,e[1].getValue());
				},500);
			}
		});
		
	}
	
	// storageService.$watchCollection("tabs",function (newVal) {
	// 	console.log("newVal tabs",newVal);
	// })
	
	storageService.codeFor = function (kind,name) {
		switch(kind) {
			case "reader":
				if(name in savedFuncAndJSONs["funcs"]){
					return savedFuncAndJSONs["funcs"][name];
				} else {
					return "function (json,index,size,returner){\n\n\t\n\n}";
				}
				break;
			case "json":
				if(name in savedFuncAndJSONs["JSONs"]){
					return savedFuncAndJSONs["JSONs"][name];
				} else {
					return "";
				}
				break;	
			case "map":
				if(name in savedFuncAndJSONs["funcs"]){
					return savedFuncAndJSONs["funcs"][name];
				} else {
					return "function (inputs,returners,done,index,size,req){\n\n\t\n\n}";
				}
				break;
			case "reduce":
				if(name in savedFuncAndJSONs["funcs"]){
					return savedFuncAndJSONs["funcs"][name];
				} else {
					return "function (parts,returner){\n\n\t\n\n}";
				}
				break;
		}
	}
	
	//Fill it when everything is good, empty it when not.
	storageService.job = {
		"jobDescription":null,
		"funcs":{},
		"JSONs":{}
	};
	
	storageService.explanationFor = function (kind) {
		switch(kind) {
			case "reader":
				return "TODO explain reader";
				break;
			case "json":
				return "TODO explain JSON";
				break;	
			case "map":
				return "TODO explain map";
				break;
			case "reduce":
				return "TODO explain reduce";
				break;
		}
	}
	
	return storageService;
}]);

//controllers

app.controller('ToolbarCtrl', ['$rootScope', '$scope', '$location', function ($rootScope, $scope, $location) {
	
}]);


app.controller('TabsCtrl', ['$rootScope', '$scope', '$location', 'storageService','$http','$uibModal','$timeout', '$interval', function ($rootScope, $scope, $location, storageService, $http, $uibModal, $timeout, $interval) {
	
	function randomID(length) {
		var str = new Array(length + 1).join("x");
		//gnu id generator
		return str.replace(/[xy]/g, function (c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}
	
	$scope.tabs = [];
	
	$scope.examples = {
		"1 Matrix (simple)" : {
			"jobDescriptionCode":"var N = 3;\n\ngroup(\"g1\",{\n  \"reader\":\"func1\",\n \"json\":\"m1.json\",\n \"size\": N*N\n})\n\ngroup(\"g2\",{\n \"reader\":\"func2\",\n \"json\":\"m2.json\",\n \"size\": N*N\n})\n\ngroup(\"g3\",{\n \"ordered\":true,\n \"size\":N*N\n})\n\nmap({\n \"inputs\":[\"g1\",\"g2\"],\n \"func\":\"func3\",\n \"outputs\":[\"g3\"]\n})\n\nreduce({\n  \"input\":\"g3\",\n \"func\":\"func4\",\n \"result\":\"result1\"\n})\n",
			"funcs":{
				"func4": "function (parts,returner){\n\tvar comb = [];\n\t\n\tfor (var i = 0; i < parts[0].length; i++) {\n\t\tif(parts[0][i] != null) comb.push(parts[0][i]);\n\t}\n\t\n\tfor (var j = 0; j < parts[1].length; j++) {\n\t\tif(parts[1][j] != null) comb.push(parts[1][j]);\n\t}\n\t\n\treturner(comb);\n}",
				"func3": "function (inputs,returners,done,index,size){\n\tvar N = Math.sqrt(size);\n\tvar m = index % N;\n\tvar n = (index - m) / N;\n\t\n\tvar a = inputs[\"g1\"];\n\tvar b = inputs[\"g2\"];\n\t\n\tvar total = 0;\n\t\n\tfor (var i = 0; i < N; i++) {\n\t\ttotal += a[i] * b[i];\n\t}\n\t\n\treturners[\"g3\"](index,[{\"n\":n,\"m\":m,\"val\":total}]);\n\tdone();\n}",
				"func2": "function (json,index,size,returner){\n\tvar N = Math.sqrt(size);\n\tvar m = index % N;\n\tvar n = (index - m) / N;\n\tvar array = [];\n\t\n\tfor (var i = 0; i < N; i++) {\n\t\tarray.push(json[i][m]);\n\t}\n\t\n\treturner(array);\n}",
				"func1": "function (json,index,size,returner){\n\tvar N = Math.sqrt(size);\n\tvar m = index % N;\n\tvar n = (index - m) / N;\n\tvar array = [];\n\t\n\tfor (var i = 0; i < N; i++) {\n\t\tarray.push(json[n][i]);\n\t}\n\t\n\treturner(array);\n}"
			},
			"JSONs":{
				"m2.json": "[[1,0,0],[0,1,0],[0,0,1]]",
				"m1.json": "[[1,2,3],[4,5,6],[7,8,9]]"
			}
		},
		"2 Stock/Tweet Analysis": {
			"jobDescriptionCode":"var N = 10;\n\ngroup(\"stocksData\",{\n  \"reader\":\"analyzeStocks\",\n \"json\":\"http://52.212.88.241:3000/stocks/\",\n \"size\": N\n})\n\ngroup(\"g3\",{\n \"ordered\":true,\n \"size\":N\n})\n\nmap({\n \"inputs\":[\"stocksData\"],\n \"func\":\"analyzeTweets\",\n \"outputs\":[\"g3\"],\n \"reqBase\":\"http://52.212.88.241:3000/tweets/\"\n})\n\nreduce({\n  \"input\":\"g3\",\n \"func\":\"reduceTogether\",\n \"result\":\"result1\"\n})\n",
			"funcs": {
				"reduceTogether": "function (parts,returner){\n\n\tvar comb = [];\n\t\n\tfor (var i = 0; i < parts[0].length; i++) {\n\t\tif(parts[0][i] != null) comb.push(parts[0][i]);\n\t}\n\t\n\tfor (var j = 0; j < parts[1].length; j++) {\n\t\tif(parts[1][j] != null) comb.push(parts[1][j]);\n\t}\n\t\n\tcomb.sort(function (a, b) {\n\t\treturn b.time - a.time;\n\t});\n\t\n\treturner(comb);\n\n}",
				"analyzeTweets": "function (inputs,returners,done,index,size,req){\n\n\t//Assumed to come with stock information.\n\tvar keywords = [\"$ba\",\"boeing\",\"stock\",\"flight\",\"airplane\",\"nyse\"];\n\n\tvar toCheck = inputs[\"stocksData\"];\n\t\n\tif (toCheck.length < 1){\n\t\t//Nothing to check case.\n\t\treturners[\"g3\"](index,[]);\n\t\tdone();\n\t\treturn;\n\t}\n\t\n\tvar results = [];\n\tvar reqLeft = toCheck.length;\n\t\n\tvar matchingString = \"\";\n\tfor (var i = 1; i < keywords.length; i++) {\n\t\tmatchingString += '|\\\\b' + keywords[i] + '\\\\b';\n\t}\n\tvar matcher = new RegExp(matchingString.substr(1));\n\t\n\ttoCheck.forEach(function (change) {\n\t\t/*\n\t\t\tchange: {\n\t\t\t\t\"price\":...,\n\t\t\t\t\"lastPrice\":...,\n\t\t\t\t\"lastDateInSec\":...,\n\t\t\t\t\"dateInSec\":...\n\t\t\t}\n\t\t*/\n\t\t\n\t\treq.get({\n\t\t\t\"query\": {\n\t\t\t\t\t\"startSec\":change.lastDateInSec,\n\t\t\t\t\t\"endSec\":change.dateInSec,\n\t\t\t\t}\n\t\t\t},\n\t\t    function (data,err) {\n\t\t    \tif (err) {\n\t\t    \t\tresults.push({\"err\":err});\n\t\t    \t} else {\n\t\t    \t\tfor (var i = 0; i < data.length;i++) {\n\t\t    \t\t\t\n\t\t    \t\t\tvar keywordFound = false;\n\t\t    \t\t\t\n\t\t    \t\t\tif(matcher.test(data[i].text.toLocaleLowerCase())){\n\t\t    \t\t\t\tkeywordFound = true;\n\t\t    \t\t\t}\n\t\t    \t\t\t\n\t\t    \t\t\tif (keywordFound) {\n\t\t    \t\t\t\tresults.push({\n\t\t\t\t\t\t\t\t\"price\":change.price,\n\t\t\t\t\t\t\t\t\"lastPrice\":change.lastPrice,\n\t\t\t\t\t\t\t\t\"lastDateInSec\":change.lastDateInSec,\n\t\t\t\t\t\t\t\t\"cateInSec\":change.cateInSec,\n\t\t\t\t\t\t\t\t\"text\":data[i].text,\n\t\t\t\t\t\t\t\t\"time\": data[i].time\n\t\t\t\t\t\t\t});\n\t\t\t    \t\t}\n\t\t    \t\t\t\n\t\t    \t\t}\n\t\t    \t}\n\t\t    \tif(--reqLeft == 0) {\n\t\t    \t\tlog(\"[sent done()]\");\n\t\t    \t\treturners[\"g3\"](index,results);\n\t\t\t\t\tdone();\n\t\t    \t}\n\t\t\t}\n\t\t)\n\t\t\n\t});\n\n}",
				"analyzeStocks": "function (rows,index,size,returner){\n\n\tvar threshold = 1.2;\n\n\tvar largeChanges = [];\n\tvar baseDateInSec,\n\t\tlastDateInSec,\n\t\tlastPrice;\n\t\t\n\tfor (var i = 0; i < rows.length; i++) {\n\t\tvar row = rows[i];\n\t\tvar parts = row.split(\",\");\n\t\t\n\t\tif(isNaN(parts[0])){\n\t\t\tif(parts[0] == \"PRE\"){\n\t\t\t\ttry{\n\t\t\t\t\tlastPrice = parseFloat(parts[1]);\n\t\t\t\t\tlastDateInSec = 0;\n\t\t\t\t\tcontinue;\n\t\t\t\t} catch (err) {\n\t\t\t\t\tlog(\"BUG 1.1\",err);\n\t\t\t\t}\n\t\t\t} else if(parts[0].indexOf(\"a\") == 0 && parts[0].length > 2){\n\t\t\t\ttry{\n\t\t\t\t\tbaseDateInSec = parseInt(parts[0].substring(1));\n\t\t\t\t\tcontinue;\n\t\t\t\t} catch (err) {\n\t\t\t\t\tlog(\"BUG 1.2\",err);\n\t\t\t\t}\n\t\t\t} else {\n\t\t\t\tlog(\"BUG 1.3\");\n\t\t\t}\n\t\t}\n\t\t\n\t\tif(parts.length < 3) {\n\t\t\tlog(\"BUG 2\");\n\t\t\tcontinue;\t\n\t\t}\n\t\t\n\t\ttry {\n\t\t\tvar closing = parseFloat(parts[1]);\n\t\t\tif (Math.abs(closing - lastPrice) > threshold) {\n\t\t\t\tvar closingDateInSecs = baseDateInSec + parseInt(parts[0]);\n\t\t\t\tlargeChanges.push({\n\t\t\t\t\t\"price\":closing,\n\t\t\t\t\t\"lastPrice\":lastPrice,\n\t\t\t\t\t\"lastDateInSec\":lastDateInSec,\n\t\t\t\t\t\"dateInSec\":closingDateInSecs\n\t\t\t\t});\n\t\t\t}\n\t\t\tlastPrice = closing;\n\t\t}  catch (err) {\n\t\t\tlog(\"BUG 3\",err);\n\t\t}\n\t}\n\t\n\treturner(largeChanges);\n\n}"
			},
			"JSONs": {}
		}
	};
	
	storageService.$watchCollection("tabs",function (newVal, oldVal) {
		$scope.tabs = storageService.tabs;
	});
	
	var setEditorText;
	$scope.onAceLoaded = function (_editor) {
		_editor.setFadeFoldWidgets(true);
		
		//First editor is Job Description
		if(!setEditorText) setEditorText = function (code) {
			_editor.setValue(code);
		}
	}
	
	$scope.loadExample = function (name) {
		//console.log($scope.examples[name]);
		
		//Load funcs and JSONs to storage.
		storageService.loadFuncNJSONs($scope.examples[name]);
		
		//Load code into editor and storage.
		var code = $scope.examples[name]["jobDescriptionCode"];
		storageService.saveJobDescriptionCode(code);
		setEditorText(code);
		
		//Update structure (and tabs).
		$rootScope.updateStructure(code);
		
	}
	
	$scope.disableRunButton = function () {
		//TODO check stuff inside the tabs as well.
		return !storageService.job["jobDescription"] || (storageService.job["jobDescription"] === null);
	}
	
	$scope.submissionID = randomID(15);
	
	$scope.updateSubmissionToken = function (token) {
		if(typeof(Storage) !== "undefined" && !!token) localStorage.submissionToken = token;
		if(!!token) $scope.submissionToken = token;
	}
	
	$timeout(function() {
		if(typeof(Storage) !== "undefined" && "submissionToken" in localStorage && !!localStorage.submissionToken){
			$scope.submissionToken = localStorage.submissionToken;
		}
	},1);
	
	$scope.submitJob = function () {
		
		if($scope.submitting) return;
		
		$scope.submitting = true;
		
		$scope.jobProgress = {};
		
		if(!$scope.submissionToken){
			localStorage.submissionToken = $scope.submissionToken = prompt("Submission Token:");
		}
		
		for (var i = storageService.tabs.length - 1; i >= 0; i--) {
			var tab = storageService.tabs[i];
			if(tab.mode == "json"){
				storageService.job["JSONs"][tab.title] = tab.code;
			} else if (tab.mode == "javascript"){
				storageService.job["funcs"][tab.title] = tab.code;
			} else {
				console.log("BUG");
			}
		}
		
		//console.log("runJob",storageService.job);
		
		$http({
			method : "POST",
			url : [
					storageService.apiAddress + "submit",
					$scope.submissionID,
					"?token="+encodeURI($scope.submissionToken)
				].join('/'),
			data : storageService.job
		}).then(function mySucces(response) {
			
			$scope.deneme = 10;
			
			$scope.submitting = false;
			var subID = $scope.submissionID;
			$scope.submissionID = randomID(15);
			
			$scope.oldSubID = subID;
			
			$scope.donePercent = 0;
			$scope.inProgressPercent = 0;
			
			var intervalForProgress = $interval(function () {
				$http({
					method : "GET",
					url : [storageService.apiAddress + "progress",subID].join('/')
				}).then(function (response) {
					if("data" in response){
						var data = response["data"];
						if(!("errorMessage" in data)){
							$scope.donePercent = Math.ceil(data.done * 100 / data.total);
							$scope.inProgressPercent = Math.ceil(data.inProgress * 100 / data.total);
						}
						$scope.jobProgress = data;
						
						if(data.total == data.done){
							$interval.cancel(intervalForProgress);
							
							$scope.results = {};
							
							console.log(storageService.job["jobDescription"]);
							
							if("jobDescription" in storageService.job && "results" in storageService.job["jobDescription"]){
								
								var resultNames = storageService.job["jobDescription"]["results"];
								
								resultNames.forEach(function(resultName) {
									$http({
										method : "GET",
										url : [storageService.apiAddress + "result",resultName,"?subID="+subID].join('/')
									}).then(function (response) {
										var data = response["data"];
										
										if(data && "Val" in data){
											
											$scope.results[resultName] = JSON.stringify(data["Val"],null,2);
											
										} else {
											console.log("Weird error on jobDescription results 2");
										}
										
									});
								});
								
							} else {
								console.log("Weird error on jobDescription results 1");
							}
							
						}
						
					}
				})
			},2000);
			
			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'submissionModal.html',
				keyboard: false,
				backdrop: 'static',
				size:'lg',
				scope: $scope
			});
			
			modalInstance.closed.then(function () {
				$interval.cancel(intervalForProgress);
				$scope.results = {};
			});
			
		}, function myError(response) {
			console.log("response.statusText",response.statusText);
			
			$scope.submitting = false;
			$scope.submissionID = randomID(15);
			
			if(response.statusText == "Bad Request"){
				alert("Check submission token and retry.");
			}
			
		});
		
	}
	
	$scope.clearAll = function () {
		// body...
		console.log("TODO clearAll");
		
	}

}]);

app.controller('JobDescriptionCtrl', ['$scope', '$rootScope', 'storageService','$timeout', function ($scope, $rootScope, storageService, $timeout) {
		
	$scope.code = storageService.jobDescriptionCode;
	
	$scope.autorun = storageService.autorun;

	$scope.aceOptions = {
		mode: "javascript",
		theme: "monokai",
		onLoad: $scope.$parent.onAceLoaded
	};
	
	var updateStructure = (function(){
				
		var funcs = [
			function deneme(x){ //TODO delete this
				console.log(x);
			},
			function group(name,conf){
				//TODO check conf first
				
				//TODO consider other configurations with reader etc.
				
				graph.nodes.forEach(function(item){
					if(item.id == name){ throw "<b>[GROUP]:</b> name '"+name+"' already exist.";}
				});
				
				//TODO check if already exists.
				
				var tempNode = {"id":name,"type":0};
				for(var k in conf){
					//TODO check config first, throw error if not valid
					tempNode[k] = conf[k];
				}
				graph.nodes.push(tempNode);
				
				//TODO update Job Description as well
				
			},
			function map(conf){
				//TODO check conf first
				
				var funcName = conf.func;
				var inputs = conf.inputs; //TODO make array if single node
				var outputs = conf.outputs; //TODO make array if single node
				
				//TODO check if already exists + check if function name exists.
				
				var tempNode = {"id":funcName,"type":3,"inputs":inputs,"outputs":outputs};
				
				if(conf.reqBase) tempNode.reqBase = conf.reqBase;
				
				var inputsFound = 0;
				graph.nodes.forEach(function(item){
					if(item.type < 3 && inputs.indexOf(item.id) > -1){
						inputsFound++;
					}
				});
				
				if(inputs.length != inputsFound) throw "<b>[MAP]:</b> One or more inputs doesn't exist.";
				
				var outputsFound = 0;
				graph.nodes.forEach(function(item){
					if(item.type < 3 && outputs.indexOf(item.id) > -1){
						outputsFound++;
					}
				});
				
				if(outputs.length != outputsFound) throw "<b>[MAP]:</b> One or more outputs doesn't exist.";
				
				graph.nodes.push(tempNode);
				
				for(var k in inputs){
					graph.links.push({"source":inputs[k],"target":funcName});
				}
				for(var k in outputs){
					graph.links.push({"source":funcName,"target":outputs[k]});
				}
				
				//TODO update Job Description as well
			},
			function reduce(conf){
				//TODO check conf first
				
				var funcName = conf.func;
				var input = conf.input;
				//var output = conf.output;
				//var times = conf.times;
				var result = conf.result;
				
				//TODO consider other kinds of config.
				
				//TODO check if already exists + check if function name exists.
				
				if(graph.results.indexOf(result) > -1) throw '<b>[REDUCE]:</b> result with name "'+result+'" already exists.';
				
				var inputFound = false;
				graph.nodes.forEach(function(item){
					
					if(item.type == 4 && input == item.input && funcName == item.id){
						throw '<b>[REDUCE]:</b> another reduce with input "'+input+'" and function "'+funcName+'" already exists.';
					} else if(item.type < 3 && input == item.id){
						inputFound = true;
					}
					
				});
				if(!inputFound) throw "<b>[REDUCE]:</b> input doesn't exist.";
				
				
				graph.nodes.push({"id":funcName,"type":4,"input":input,"result":result});
				graph.nodes.push({"id":result,"type":5});
				
				graph.links.push({"source":input,"target":funcName});
				graph.links.push({"source":funcName,"target":result});
				
				//results array helps organize the visual graph.
				graph.results.push(result);
				
			},
		]
		
		var funcText = funcs.map(function(func){
			return func.toString();
		}).join('\n');
		
		var preText = "var graph = {'nodes':[],'links':[],'results':[]};";
		
		var oldResulText;

		return function (text) {

			//TODO Do we need to check for "for" "while" loops etc in text ?
			//TODO Do we need to check for "if" "else" in text ?
			
			
			try {
				var result = eval("(function(){" + preText + funcText + "\n" + text + "\nreturn graph;})();");
				
				var tempResultText = JSON.stringify(result);
				
				if(oldResulText && oldResulText == tempResultText){
					$scope.nodeDetails = '(Code resulted in same graph)';
					return;
				}
				
				oldResulText = tempResultText;
				
				$scope.graph = result;
				
				var temp = false;
				
				var hasCycle = function(path,current){
					result.links.forEach(function(item){
						if(temp) return;
						if(item.target == current){		
							if(path.indexOf(item.source) > -1) {
								console.log("hasCycle:",path,item);
								temp = true;
								return;
							}
							var temp1 = hasCycle(path + "\n" + current,item.source);
							if (temp1){
								temp = true;
								return;
							}
						}
					});
				}
				
				var usedIDs = [];
				var usedNodeMarker = function (current) {
					usedIDs.push(current);
					result.links.forEach(function(item){
						if (item.target == current) {
							usedNodeMarker(item.source);
						}
					});
				}
				
				var cycle = false;
				for(var k in result.results){
					if(hasCycle("",result.results[k])){
						cycle = true;
						break;
					}
					
					usedNodeMarker(result.results[k]);
					
				}
				
				console.log(usedIDs);
				
				var unused = false;
				for (var i = result.nodes.length - 1; i >= 0; i--) {
					if(usedIDs.indexOf(result.nodes[i].id) < 0){
						switch(result.nodes[i].type){
							case 0:
								unused = "<b>[Error]:</b> Group <b>" + result.nodes[i].id +"</b> is not used for any of the results.";
								break;
							case 3:
								unused = "<b>[Error]:</b> Map (<b>" + result.nodes[i].id +"</b>) is not used for any of the results.";
								break;
							case 4:
								unused = "<b>[Error]:</b> Reduce (<b>" + result.nodes[i].id +"</b>) is not used for any of the results.";
								break;
						}
						break;
					}
				}
				
				//TODO remove unused parts.
				
				if(cycle){
					
					//$scope.graph = undefined;
					
					storageService.job["jobDescription"] = null;
					
					$scope.nodeDetails = "<b>Job Description has a cycle in graph</b><br>Check if you used a group for it's creation.";
					
					
				} else if (unused !== false) {
					
					//$scope.graph = undefined;
					
					storageService.job["jobDescription"] = null;
					
					$scope.nodeDetails = unused;
					
				} else {
					
					storageService.clearTabs();
					
					for (var i = 0; i < result.nodes.length; i++) {
						
						//console.log(result.nodes[i]);
						
						if("reader" in result.nodes[i] && "json" in result.nodes[i]){
							storageService.addTab("reader",result.nodes[i]["reader"]);
							storageService.addTab("json",result.nodes[i]["json"]);
						} else if (result.nodes[i].type == 3){
							storageService.addTab("map",result.nodes[i].id);
						} else if (result.nodes[i].type == 4){
							storageService.addTab("reduce",result.nodes[i].id);
						}
						
					}
					
					storageService.job["jobDescription"] = angular.copy(result);
					
				}
				
			} catch (error){
				
				oldResulText = " ";
				
				console.log(error);
				
				$scope.graph = undefined;
				
				$scope.nodeDetails = error;
				
				storageService.job["jobDescription"] = null;
				
			}	
		}
	})();
	
	$rootScope.updateStructure = updateStructure;
	
	$scope.updateFromEditor = function () {
		//console.log($scope.code);
		updateStructure($scope.code);
	}
	
	var timeout;
	$scope.$watch('code',function(newValue, oldValue) {
		$timeout.cancel(timeout);
		
		if($scope.autorun) $scope.nodeDetails = '';
		
		timeout = $timeout(function() {
			
			storageService.saveJobDescriptionCode($scope.code);
			
			if($scope.autorun) $scope.updateFromEditor();
			
		}, 500);
	});
	
	$scope.$watch('autorun',function(newValue, oldValue) {
		storageService.saveAutorun($scope.autorun);
	})
	
	$scope.showNodeData = function (node) {
		//console.log(node);
		//TODO fix object attributes with human readable ones.
		
		var doNotShow = ["x","y","type","index","vy","vx","fx","fy","json"];
		
		var temp = "";
		for(var k in node){
			if(doNotShow.indexOf(k) > -1) continue;
			if(k == "id"){
				temp += '<h4 style="margin-bottom:0;">' + node[k];
				switch(node["type"]){
					case 0:
						if("json" in node){ //sourced case.
							var url = node["json"]
							var lastslash = url.lastIndexOf("/");
							var dir, file;
							if(lastslash > 0){
								dir = url.substring(0,lastslash+1);
								file = url.substring(lastslash+1);
							} else {
								dir = '';
								file = url;
							}
							
							temp += ' ("'+node["reader"]+'" reads from "<span style="text-overflow:ellipsis; max-width:20px;">'+dir+'</span>'+file+'")';
						} else {
							temp += " (Group)";
						}
						break;
					case 1:
						temp += " (Single)";
						break;
					case 2:
						temp += " (Range)";
						break;
					case 3:
						temp += " (Map)";
						break;
					case 4:
						temp += " (Reduce)";
						break;
				}
				temp += '</h4><br>';
			} else {
				temp += ['<b>',k,'</b> : ',node[k],' '].join('');
			}
		}
		
		if(temp.length > 0) $scope.$apply(function () {
            $scope.nodeDetails = temp;
        });
	}
		
}]);


//directives

app.directive('theGraph', ['$window', function($window){
	return {
		restrict:'E',
		scope: true,
		template:'<svg width="540" height="540"></svg>',
		link: function(scope, elem, attrs) {
			
			var d3 = $window.d3;
			var rawSvg=elem.find('svg');

			scope.$watchCollection("graph", function(newVal, oldVal){
				updateGraph(newVal);
			});
			
			var color = (function(){
				var colorCodes = ["#3288bd","#66c2a5","#abdda4","#e6f598", /*"#ffffbf",*/ "#fee08b",/*"#fdae61"*/"#f46d43","#d53e4f","#9e0142","#5e4fa2"];
				return function(i){
					return colorCodes[i % (colorCodes.length)];
				}
			})();
			
			var node, link, svg, simulation;
			
			function ticked() {
				node
					.attr("cx", function(d) { return d.x; })
					.attr("cy", function(d) { return d.y; });
				// 	.attr("transform", function(d) {
				// 		return "";
				// 	});
				
				// link
				// 	.attr("x1", function(d) { return d.source.x; })
				// 	.attr("y1", function(d) { return d.source.y; })
				// 	.attr("x2", function(d) { return d.target.x; })
				// 	.attr("y2", function(d) { return d.target.y; });
				
				link.attr("x1", function(d) { return d.source.x; })
					.attr("y1", function(d) { return d.source.y; })
					.attr("x2", function(d) { return d.target.x; })
					.attr("y2", function(d) { return d.target.y; });

    			node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

			}
			
			function dragstarted(d) {
				if(d.type == 5) return;
				
				if (!d3.event.active) simulation.alphaTarget(0.3).restart();
				d.fx = d.x;
				d.fy = d.y;
			}

			function dragged(d) {
				if(d.type == 5) return;
				d.fx = d3.event.x;
				d.fy = d3.event.y;
			}

			function dragended(d) {
				if(d.type == 5) return;
				if (!d3.event.active) simulation.alphaTarget(0);
				d.fx = null;
				d.fy = null;
			}
			
			function updateGraph(temp) {
				
				while (rawSvg[0].firstChild) {
					rawSvg[0].removeChild(rawSvg[0].firstChild);
				}
				
				if(!temp) return;
				
				svg = d3.select(rawSvg[0]);
				var	 width = +svg.attr("width"),
					height = +svg.attr("height");
				
				var index = 0;
				for(var k in temp.nodes){
					if(temp.results.indexOf(temp.nodes[k].id) < 0) continue;
					
					temp.nodes[k].fy = height - 40;
					temp.nodes[k].fx = width/(temp.results.length + 1)* ++index;
				}
				
				simulation = d3.forceSimulation()
					.force("link", d3.forceLink().distance(80).id(function(d) { return d.id; }))
					.force("charge", d3.forceManyBody())
					.force("center", d3.forceCenter(width / 2, height / 2));
				
				var circleRadius = 5 + Math.abs(15 - 2*Math.log(temp.nodes.length));
				if (circleRadius > 20) circleRadius = 20;
				
				svg.append("svg:defs").selectAll("marker")
				    .data(["end"])      // Different link/path types can be defined here
				  .enter().append("svg:marker")    // This section adds in the arrows
				    .attr("id", String)
				    .attr("viewBox", "0 -5 10 10")
				    .attr("refX", 16)
				    .attr("refY", 0)
				    .attr("markerWidth", circleRadius/4)
				    .attr("markerHeight", circleRadius/4)
				    .attr("orient", "auto")
				  .append("svg:path")
				    .attr("d", "M0,-5L10,0L0,5");
				    
				link = svg.append("g")
					.selectAll("line")
					.data(temp.links)
					.enter().append("line")
					.attr("class", "links")
					.attr("marker-end", "url(#end)");
					//.attr("stroke-width", function(d) { return 2;});//Math.sqrt(d.value); });

				node = svg.selectAll(".node")
						.data(temp.nodes)
					.enter().append("g")
						.attr("class", "node")
    					.on('click' , scope.showNodeData)
    					.on('mouseover' , scope.showNodeData);
					
				node.append("circle")
					.attr("r", circleRadius)
					.attr("fill", function(d) {return color(d.type);})
					.attr("class", function (d) {
						if (d && "type" in d){
							if (d.type >= 3 && d.type != 5) {
								return "task-node";
							} else {
								return "data-node";
							}	
						} else {
							return "false-node";
						}
					})
					.call(d3.drag()
						.on("start", dragstarted)
						.on("drag", dragged)
						.on("end", dragended));

				node.append("text")
					.attr("class", function (d) {
						return "nodeText t"+d.type;
					})
					.attr("text-anchor", "middle")
      				.attr("dy", ".35em")
					.text(function(d) { return d.id; });
				
				simulation
					.nodes(temp.nodes)
					.on("tick", ticked);

				simulation.force("link")
					.links(temp.links);
				
			}
			
			elem.on('$destroy', function() {
      			//TODO
    		});
			
		}
	}
}]);