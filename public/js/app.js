'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('main', [
	'ngRoute',
	'ui.bootstrap',
	'ngStorage',
	'ngNotify',
	]).
config(['$routeProvider', '$sceProvider', function($routeProvider,$sceProvider) {

	$routeProvider.when('/home', {
		templateUrl: 'templates/home.html',
		controller: 'HomeCtrl'
	}).when('/updates', {
		templateUrl: 'templates/updates.html',
		controller: 'UpdatesCtrl'
	}).when('/contact', {
		templateUrl: 'templates/contact.html',
		controller: 'ContactCtrl'
	})
	$routeProvider.otherwise({redirectTo: '/home'});
	
	$sceProvider.enabled(false);
}]);

//services

//controllers

app.controller('ToolbarCtrl', ['$rootScope', '$scope', '$location',
	function ($rootScope, $scope, $location) {
		$scope.path = $location.path();
		$scope.$on('$locationChangeSuccess', function(){
  			$scope.path = $location.path();
        ga('send', 'pageview');
		});
		
		$scope.openingDemo= function(){
			ga('send', 'event', 'navbar', 'click', 'demo' );
		};
		
		$scope.openingHelper= function(){
			ga('send', 'event', 'navbar', 'click', 'helper' );
		};
		
	}]);

app.controller('HomeCtrl', ['$rootScope', '$scope', '$routeParams', '$route', '$location', '$http',
	function ($rootScope, $scope, $routeParams, $route, $location, $http) {
		$rootScope.appTitle = "Home";
	}]);

app.controller('UpdatesCtrl', ['$rootScope', '$scope', '$routeParams', '$route', '$location', '$http','$q',
	function ($rootScope, $scope, $routeParams, $route, $location, $http,$q) {
		$rootScope.appTitle = "Loading...";
		
		$http.get('./posts.json').then(function(response){

			if("data" in response && response.data){
				
				var data = response.data;
				
				$scope.downloadedPosts = new Array(data.length);
				
				var reqs = [];
				
				var postsLeft = data.length;
				for (var i in data) {
					reqs.push($http.get('./posts/'+ data[i] +'.html', {cache: false}));
				}
				
				$q.all(reqs).then(function(values) {
					$rootScope.appTitle = "Updates";
					for(var i in values){
						$scope.downloadedPosts[i] = values[i].data;
					}
				},function(){
					//TODO: Put one big error message.
				});
				
			} else {
				//TODO Put one big error message.
			}
			
		}, function(){
			//TODO Put one big error message.
		});

			
			
		}]);

app.controller('ContactCtrl', ['$rootScope', '$scope', '$routeParams', '$route', '$location', '$http',
	function ($rootScope, $scope, $routeParams, $route, $location, $http) {
		$rootScope.appTitle = "Contact";
	}]);


//directives


