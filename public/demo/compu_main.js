var showLogs = true;
(function function_name() {
	
	//TODO recheck for mobile
	var vcores = navigator.hardwareConcurrency || (!!navigator.userAgent.match(/iPhone/i) || !!navigator.userAgent.match(/Android/i) ? 2 : 4 );
	var coresToUse = (vcores > 2 ? vcores -1 : 1);
	
	//TODO remove when minimizing.
	var apiAddress = (location.hostname == "localhost" ? "http://localhost:5000/api" : location.origin + "/api");
	
	var storageTypeToUse = 0;
	if(typeof Storage !== "undefined"){
		try {
			localStorage.setItem("compujs",1);
			storageTypeToUse = 1;
		} catch (e) {
			//TODO fallback
			storageTypeToUse = 2;
		}
	}
	console.log("storageTypeToUse",storageTypeToUse);
	
	var save = (function(){
		if(storageTypeToUse == 1){
			return function(key,value){
				localStorage.setItem(key,value);
			};
		} else {
			return function(){};
		}
	})();
	var load = (function(){
		if(storageTypeToUse == 1){
			return function(key){
				var str = localStorage.getItem(key);
				try {
					return JSON.parse(str);
				} catch (e){
					return str;
				}
			};
		} else {
			return function(){};
		}
	})();
	
	var logDiv = document.getElementById('logOutput');
	var logOutputWrapper = document.getElementById('logOutputWrapper');
	var log = function(text) {
		var wasAtBottom = logOutputWrapper.scrollTop >= (logOutputWrapper.scrollHeight - logOutputWrapper.offsetHeight);
		logDiv.innerHTML += text.replace("[","<span style='font-weight:bold;'>[").replace("]","]</span>") + "<br>";
		if(wasAtBottom) logOutputWrapper.scrollTop = logOutputWrapper.scrollHeight;
	};
	
	var workerJSURL = "./compu_worker.js";
	// try {
	// 	workerJSURL = window.URL.createObjectURL(new Blob(["console.log('Hello, world!')"],{type: 'application/javascript'}));
	// } catch (e){}
	
	var workerid = 0;
	function spawnWorker(i){
		var worker = new Worker(workerJSURL);
		//TODO Transferrable objects
		
		worker.onmessage = function(e) {
      		//log("Worker"+i+" sent : " + e.data);
      		//console.log(e.data[0]);
      		switch(e.data[0]){
      			case 0:
      				//console.log("save");
      				save(e.data[1],e.data[2]);
      				break;
      			case 1:
      				//console.log("load");
      				var temp = load(e.data[1]);
      				workers[i].postMessage([1,e.data[1],temp]);
      				break;
      			case 2:
      				//console.log("log");
      				if(showLogs) log("Worker"+i+": "+e.data[1]);
      				break;
      			case 3:
      				var jid = e.data[1];
      				var func = e.data[2];
      				save("worker"+i,jid);
      				save(jid,func);
      				break;
      			// case "a": //Analytics
      			// 	console.log("save");
      			// 	break;
      		}
    	};
    	worker.onerror = function (e) {
    		console.log("e.message,e.lineno",e.message,e.lineno);
    		//TODO send error to server.
    		//save("worker"+i,0);
    		workers[i] = undefined;
    		workers[i] = spawnWorker(i);
    	};
    	
    	//TODO get proper parent id etc.
    	
    	var jid = load("worker"+i);
    	if(jid && jid != "0"){
    		//Set worker up with saved job
    		console.log("start from saved");
    		worker.postMessage([1,"xxxxxx",apiAddress, workerid++,jid,load(jid)]);
    	} else {
    		//Set worker up.
    		worker.postMessage([0,"xxxxxx",apiAddress, workerid++]);
    	}
		
		return worker;
	}
	
	var workers = new Array(coresToUse);
	for(var i=0; i<coresToUse; i++){
		workers[i] = spawnWorker(i);
	}
	
})();