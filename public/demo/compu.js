(function function_name() {
	
	//TODO override .toStrings for all callbacks. Hide everything you can hide.
	
	//TODO check browser compability.
	
	//TODO add checks for all callbacks to see if functions.
	
	//TODO give website developers option to include new overrides, for jQuery etc.
	
	//TODO init.
	//TODO check local storage and other storage options, try to find client id (TODO and job info later)
	
		
	//TODO make it hardcoded.
	var apiAddress = (location.hostname == "localhost" ? "http://localhost:5000/api/" : "http://compujs.com/api/");
	
	var debugMode = 2; //Remove this, one day :)

	var head = document.getElementsByTagName('head')[0];
	
	var logDiv = document.getElementById('logOutput');

	//log
	function log(text){
		if(debugMode >= 1) console.log(text);
		if(debugMode >= 2){
			logDiv.innerHTML += text.replace("[","<span style='font-weight:bold;'>[").replace("]","]</span>") + "<br>";
		}
	}

	//TODO, add XHR as argument, have a queue and wait for internet connection event or smt.
	function connectionError(){
		console.error("[Compujs] " + new Date());
		if(debugMode >= 2) logDiv.innerText += "[connectionError]\n";
	}

	//Reference: $script.js
	//BROWSER GET Script Loader: client id, job id OR milliseconds for re-init timeout, callback function (used if script loaded)
	function sl(cid,jidORms,fn) {
		
		log("[sl] cid="+cid+" jidORms="+jidORms);
		
		if(!(jidORms && jidORms.length>27 && /^[a-zA-Z]+$/.test(jidORms))){
			
			var toms = 10000; //TimeOut MiliSecond, 10 sec

			try {
				var temp = Number(data);
				//Lowest acceptable time is 1 sec.
				if (!isNAN(temp) &&temp > 1000) toms = temp;
			} catch (e) {
				//TODO better error.
			}
			
			//TODO remove
			console.timeEnd("tinyDemo");
			
			setTimeout(function () {
				initHelper(cid);
			}, toms);
			
			return;
		}
		
		// If this line is reached, jidORms is defined, longer than 28 char and alpha only.
		var jid = jidORms;
		
		var element = document.createElement('script'), loaded;
		
		element.src = apiAddress + "/" + encodeURI(jid) + '?cid=' + encodeURI(cid);
		
		element.onload = element.onerror = element['onreadystatechange'] = function () {
			
			//TODO check error.
			
			if ((element['readyState'] && !(/^c|loade/.test(element['readyState']))) || loaded) return;
			
			element.onload = element['onreadystatechange'] = null
			loaded = 1
			
			head.removeChild(element); //TODO fix.
			
			var func = window[jid];
			
			//TODO find better way.
			window[jid] = undefined;
			
			fn(func);
				
		}
			
		element.async = 1
		
		head.insertBefore(element, head.lastChild) //Basically appending to head
	}

	//TODO loaded script should define a new variable with id in src
	//TODO have loadNextTask and initClient base on load ("Bu ne ?")

	//GET initial request: client id recovered, callback function
	function initHelper(cid){
		
		var request = new XMLHttpRequest();
		request.open('GET', apiAddress + (cid && cid.length>0 ? '?cid=' + encodeURI(cid) : ''), true);

		request.onload = function() {
			if (request.status >= 200 && request.status < 400 && request.responseText && request.responseText.length>0) {
				var data = JSON.parse(request.responseText);
				log("[initHelper] data="+ data);
				
				var jidinitial = data[1];
				var cidinitial = data[0];
				
				sl(cidinitial, jidinitial, function(func) {
					
					
					var returner = returnerHelper(cidinitial,jidinitial);
					func(returner);
					
				});
			} else {
				// Server returned error or unextected responce.
				//TODO
				
				connectionError();
			}
		};

		request.onerror = function() {
			// There was a connection error of some sort
		};

		request.send();
	}
		
	//POST sends job result back:
	function pback(cid,jid,dataToSendBack,success){
		
		log("[pback] cid="+cid+" jid="+jid+" dataToSendBack="+dataToSendBack);
		
		var request = new XMLHttpRequest();
		request.open('POST', apiAddress + encodeURI(jid) + '?cid='+encodeURI(cid));
		request.setRequestHeader('Content-Type', 'application/json');
		request.onload = function() {
				if (request.status === 200) {

						var text = request.responseText;
						success(text);
						
				} else {
					//TODO handle error.
				}
		};
		
		request.send(JSON.stringify(dataToSendBack));
		
	}
	
	// Returns a function that sends results back with pback and loads next job with sl: client id, finished job id, job's resulting data
	function returnerHelper(cid,jid){
		
		log("[returnerHelper] cid="+cid+" jid="+jid);
		
		return function(resultingData){
			
			pback(cid, jid, resultingData, function(data){
				
				var nextJobIdORms = data;
				sl(cid, nextJobIdORms, function (func){
					
					var returner = returnerHelper(cid,nextJobIdORms);
					func(returner);
					
				});
				
			});
			
		}
	}
	
	function init() {
		
		//TODO send cid if saved before instead of ''
		var cid = '';
		
		log("[init] cid="+cid);
		
		initHelper(cid);
	}
	
	//PLAN
	//
	//Ask for a job with GET. Load job using returned job id (with sl).
	//
	//Job comes with data in js file as well.
	//Job returns results with first arg given. POST (include job id, client id), in return we get new job id or a number for how long to wait.
	//
	//Repeat :)
	
	console.time("tinyDemo");
	init();
	
})();