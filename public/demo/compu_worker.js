var fakeStorage = {};
var parentid, apiAddress, workerid;

function log(text){
	console.log(text);
	postMessage([2,text]);
}

function connectionError(text) {
	log("[connectionError] " + text);
}

// function sl(cid,jidORms,fn) {
	
// 	log("[sl] cid="+cid+" jidORms="+jidORms);
	
// 	// If this line is reached, jidORms is defined, longer than 28 char and alpha only.
// 	var jid = jidORms;
	
// 	importScripts(apiAddress + "/" + encodeURIComponent(jid) + '?cid=' + encodeURIComponent(cid));
	
// 	var func = self[jid];
	
// 	//TODO send function and data back to parent/master seperately.
// 	//postMessage([0,jid,func.toString()]);
	
// 	fn(func);
	
// }

var queryHelper = function (base,query) {
	var url = ""+base;
	var keys = [];
	var values = [];
	for(var key in query){
		keys.push(key);
		values.push(query[key]);
	}
	if(keys.length == 0) return url;
	if(base.indexOf("?") > 0){
		url += ["&",keys[0],"=",values[0]].join('');
	} else {
		url += ["?",keys[0],"=",values[0]].join('');
	}
	if(keys.length == 1) return url;
	for (var i = 1; i < keys.length; i++) {
		url += ["&",keys[i],"=",values[i]].join('');
	}
	return url;
}


function getReqHelper(url,preResolve){
	
	//TODO cid, jid
	
	return new Promise((resolve, reject) => {
		var request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.onload = function() {
			if (request.status >= 200 && request.status < 400 && request.responseText && request.responseText.length>0) {
				resolve(preResolve(request.response));
			} else {
				connectionError();
				
				console.log("onload else",{
					url:url,
					status: this.status,
					statusText: request.statusText
				});
				
				reject({
					url:url,
					status: this.status,
					statusText: request.statusText
				});
			}
		};
		request.onerror = function () {
			connectionError();
			
			console.log("onerror",{
					url:url,
					status: this.status,
					statusText: request.statusText
				});
			
			reject({
				url:url,
				status: this.status,
				statusText: request.statusText
			});
		};
		request.send();
	});
}

var storedFuncs = {};
function getFunc(funcName,subID){
	
	log("[getFunc] " + funcName);
	
	//TODO cid, jid
	
	if(funcName in storedFuncs){
		return new Promise((resolve, reject) => { resolve(storedFuncs[funcName]); });
	} else {
		return new Promise((resolve, reject) => {
			
			try {
			    importScripts( apiAddress + "/js/" + encodeURIComponent(funcName) + "/?subID=" + encodeURIComponent(subID) );
			}
			catch(err) {
				log("[getFunc] importScripts" + err);
			    reject("getFunc importScripts error:" + err);
			    return;
			}
			
			if(funcName in self){ //TODO check for other stuff.
				
				storedFuncs[funcName] = self[funcName];
				
				//TODO send function back to parent/master
				//postMessage();
				
				resolve(self[funcName]);
				
			} else {
				reject("getFunc load error");
			}
			
		});
	}
	
}

function getJSONReq(fileName,index,size,subID){
	
	log("[getJSONReq] " + fileName);
		
	//TODO send data back to parent/master
	//postMessage();
	
	if(fileName.indexOf("http") == 0){ //Allowing urls (assumed to be checked in server) to be accessed.
		return getReqHelper(queryHelper(fileName,{"index":encodeURIComponent(index),"size":size}),JSON.parse);
	} else {
		return getReqHelper(apiAddress + "/json/" + fileName + "/?subID=" + encodeURIComponent(subID),JSON.parse);
	}
}

function getGroupPartReq(gname,part,subID){
	
	log("[getGroupPartReq] " + gname + " " + part);
		
	//TODO send data back to parent/master
	//postMessage();
	
	return getReqHelper(apiAddress + '/g/'+gname+'/'+part + "/?subID=" + encodeURIComponent(subID) ,JSON.parse);
}

function getReducePartReq(jid,resultName,partID,subID){
	
	log("[getReducePartReq] " + jid + " " + resultName + " " + partID);
		
	//TODO send data back to parent/master
	//postMessage();
	
	return getReqHelper(apiAddress + '/r/'+ encodeURIComponent(resultName) + '/' + partID +'/?jid='+encodeURIComponent(jid) + "&subID=" + encodeURIComponent(subID) ,JSON.parse);
}


function finishJobHelper(jid,callback) {
		var request = new XMLHttpRequest();
		request.open('GET', apiAddress + '/done?jid=' + encodeURIComponent(jid), true);
		request.onload = function() {
			if (request.status >= 200 && request.status < 400 && request.responseText && request.responseText.length>0) {
				callback();
			} else {
				connectionError();
				//TODO error
			}
		};
		request.onerror = function () {
			connectionError();
			//TODO error
		};
		request.send();
		
		getNewJob();
		
}


var cid;
function getNewJob(){
	
	//TODO timeout in case dev didn't call done?
	
	var request = new XMLHttpRequest();
	request.open('GET', apiAddress + (cid && cid.length>0 ? '?cid=' + encodeURIComponent(cid) : ''), true);

	request.onload = function() {
		if (request.status >= 200 && request.status < 400 && request.responseText && request.responseText.length>0) {
			var data = JSON.parse(request.responseText);
			//log("[getNewJob] data="+ data);
			
			var cid = data.cid; //TODO save
			
			if("timeout" in data){
				log("[getNewJob] timeout="+data["timeout"]);// + "TODO uncomment.");
				setTimeout(function() {
					getNewJob(cid);
				}, data["timeout"]);
			} else if("job" in data){
				
				var job = data.job;
				
				log("[getNewJob] job="+JSON.stringify(job));
				
				switch(job.jobType) {
					case "map":
						
						var index = job.part;
						var size = job.size;
						
						var promises = [];
						promises.push(getFunc(job.func,job.subID));
						
						for (var i = 0; i < job.inputGroups.length; i++) {
							promises.push(getGroupPartReq(job.inputGroups[i],job.part,job.subID));
						}
						
						Promise.all(promises).then(values => {
							//console.log(values);

							var func = values[0];
							var inputs = {};

							for (var i = 1; i < values.length; i++) {
								inputs[job.inputGroups[i-1]] = values[i];
							}

							var returners = {};
							for (var i = 0; i < job.outputGroups.length; i++) {
								
								returners[job.outputGroups[i]] = mapJobReturnHelper(job.jid,job.outputGroups[i],job.subID);
								
							}

							var jid = job.jid;

							var done = function () {
								//TODO check if returners successfuly sent data back.
								finishJobHelper(jid,function() {
									log("[done] sent done for map job " + job.func + " " + job.part + " " + JSON.stringify(job.inputGroups) + " " + JSON.stringify(job.outputGroups));
								});
							}

							console.log("map,inputs,index,size",inputs,index,size);
							
							var req = {};
							if(job.reqBase){
								req.GET = req.get = function (conf,success,fail) {
									var url = queryHelper(job.reqBase,conf.query);
									var request = new XMLHttpRequest();
									request.open('GET', url, (conf.async !== false ? true : false));
									request.onload = function() {
										if (request.status >= 200 && request.status < 400 && request.responseText && request.responseText.length>0) {
											log("req.GET success " + url + " " + request.responseText);
											if(!success) return;
											try {
												success(JSON.parse(request.response));
											}
											catch(err) {
												log("[catch] get on load");
												success(request.response);
											}
										} else {
											log("req.GET onload fail " + url + " " + request.responseText + " " + request.status);
											if(fail) fail(request.status,request.statusText);
										}
									};
									request.onerror = function () {
										log("req.GET onerror fail " + url + " " + request.responseText + " " + request.status);
										if(fail) fail(request.status,request.statusText);
									};
									request.send();
								};
								req.POST = req.post = function (conf,success,fail) {
									var url = queryHelper(job.reqBase,conf.query);
									var request = new XMLHttpRequest();
									request.open('POST', url, (conf.async !== false ? true : false) );
									request.setRequestHeader('Content-Type', conf.contentType || 'application/json');
									request.onload = function() {
										if (request.status >= 200 && request.status < 400 && request.responseText && request.responseText.length>0) {
											log("req.POST success " + url + " " + request.responseText);
											if(!success) return;
											try {
												success(JSON.parse(request.response));
											}
											catch(err) {
												log("[catch] post on load");
												success(request.response);
											}
										} else {
											log("req.POST onload fail " + url + request.responseText + " " + request.status + " \n" + JSON.stringify(conf,null,2) );
											if(fail) fail(request.status,request.statusText);
										}
									};
									request.onerror = function() {
										log("req.POST onerror fail " + url  + request.responseText + " " + request.status + " \n" + JSON.stringify(conf,null,2));
										if(fail) fail(request.status,request.statusText);
									};
									
									if("data" in conf){
										if(conf.contentType && conf.contentType != 'application/json'){
											request.send(conf.data);
										} else {
											try {
												request.send(JSON.stringify(conf.data));
											} catch(err) {
												log("[catch] post sending");
												request.send(conf.data);
											}
										}
									} else {
										request.send();
									}
									
								};
							}

							func(inputs,returners,done,index,size,req);

						}).catch(reason => {
							log("[getNewJob] FAIL map");
						  	console.log(reason);
						});
						
						break;
					case "read":
					
						var index = job.part;
						var size = job.size;
						
						var promises = [];
						promises.push(getFunc(job.func,job.subID));
						promises.push(getJSONReq(job.json,index,size,job.subID));
						
						//TODO returners
						var returner = readJobReturnHelper(job.jid,job.group,job.part,job.subID);
						
						Promise.all(promises).then(values => {
							var func = values[0];
							var json = values[1];
							
							console.log("read func,json,index,size",func,json,index,size);
							
							func(json,index,size,returner);
							
						}).catch(reason => {
							log("[getNewJob] FAIL read");
						  	console.log(reason);
						});
						
						
						break;
					case "reduce":
					
						var promises = [];
						promises.push(getFunc(job.func,job.subID));
						promises.push(getReducePartReq(job.jid,job.resultName,job.parts[0],job.subID));
						promises.push(getReducePartReq(job.jid,job.resultName,job.parts[1],job.subID));
						
						Promise.all(promises).then(values => {
							
								var func = values[0];
								
								var part1 = values[1];
								var part2 = values[2];
								
								var returner = reduceReturnHelper(job.jid,job.resultName,job.subID);
								
								func([part1,part2],returner);
							
							}).catch(reason => {
								log("[getNewJob] FAIL reduce");
						  		console.log(reason);
							});
						
						break;
					case "reduceInit":
						
						var promises = [];
						promises.push(getFunc(job.func,job.subID));
						
						for (var i = 0; i < job.parts.length; i++) {
							promises.push(getGroupPartReq(job.inputGroup,job.parts[i],job.subID));
						}
						
						Promise.all(promises).then(values => {
							
							var func = values[0];
							
							var parts = [];

							for (var i = 1; i < values.length; i++) {
								parts.push(values[i]);
							}
							
							var returner = reduceReturnHelper(job.jid,job.resultName,job.subID);
							
							func(parts,returner);
						
						}).catch(reason => {
							log("[getNewJob] FAIL reduceInit");
						  	console.log(reason);
						});
						
						
						break;
					default:
						log("[getNewJob] Error: Unknown job type");
				}
				
			} else {
				log("[getNewJob] Error: Wrong init return");
			}
			
			// var job = data.job;
			// var cid = data.cid;
			
			// sl(cid, jidinitial, function(func) {
				
				
			// 	var returner = returnerHelper(cid,jidinitial);
			// 	func(returner);
				
			// });
		} else {
			// Server returned error or unextected responce.
			//TODO
			console.log("TODO error 1");
		}
	};

	request.onerror = function() {
		// There was a connection error of some sort
		console.log("TODO error 2");
	};

	request.send();
}

function postReqHelper(url,dataToSendBack,success){
		
	//log("[postReqHelper] url="+url+" dataToSendBack="+JSON.stringify(dataToSendBack));
	
	var request = new XMLHttpRequest();
	request.open('POST', url);
	request.setRequestHeader('Content-Type', 'application/json');
	request.onload = function() {
			if (request.status === 200) {

					var text = request.responseText;
					if(success) success(text);
					
			} else {
				//TODO do not let done before re-sending data.
				console.error("TODO handle error 1");
			}
	};
	request.onerror = function() {
		//TODO do not let done before re-sending data.
		// There was a connection error of some sort
		console.log("TODO handle error 2");
	};
	
	request.send(JSON.stringify(dataToSendBack));
	
}

function readJobReturnHelper(jid,gname,part,subID){
	//Done is only used for reader jobs, optional.
	
	return function(resultingData){
		
		log("[readJobReturnHelper] jid="+jid + " group=" + gname + " part= " + part + " resultingData=" + JSON.stringify(resultingData,null,2));
		
		postReqHelper(apiAddress + '/g/'+ encodeURIComponent(gname) +'/'+ encodeURIComponent(part) +'/?jid='+encodeURIComponent(jid) + "&subID=" + encodeURIComponent(subID), resultingData, function (response) {
			
			finishJobHelper(jid,function() {
				log("[done] sent done for read job " + jid + " " + gname + " " + part);
			});
			
		});
		
	}
}

function mapJobReturnHelper(jid,gname,subID){
	//Done is only used for reader jobs, optional.
	
	return function(part,resultingData){
		
		log("[mapJobReturnHelper] jid="+jid + " group=" + gname + " part= " + part+ " resultingData=" + JSON.stringify(resultingData,null,2));
		
		postReqHelper(apiAddress + '/g/'+ encodeURIComponent(gname) +'/'+ encodeURIComponent(part) +'/?jid='+encodeURIComponent(jid) + "&subID=" + encodeURIComponent(subID), resultingData);
		
	}
}

function reduceReturnHelper(jid,resultName,subID){
	//Done is only used for reader jobs, optional.
	
	return function(resultingData){
		
		log("[reduceReturnHelper] resultName="+ resultName + " resultingData=" + JSON.stringify(resultingData,null,2));
		
		postReqHelper(apiAddress + '/r/'+ encodeURIComponent(resultName) +'/?jid='+encodeURIComponent(jid) + "&subID=" + encodeURIComponent(subID), resultingData, function (response) {
			
			finishJobHelper(jid,function() {
				log("[done] sent done for reduce job " + jid + " " + resultName);
			});
			
		});
		
	}
}

function init() {
	
	log("[init] parentid="+parentid);
	
	cid = parentid+workerid;
	
	getNewJob();
	
}

onmessage = function(e) {
	console.log(e.data);
	switch(e.data[0]){
		case 0:
			parentid = e.data[1];
			apiAddress = e.data[2];
			workerid = e.data[3];
			init();
			break;
		case 1:
			//Get back crashed job.
			console.log("Get back crashed job.");
			parentid = e.data[1];
			apiAddress = e.data[2];
			workerid = e.data[3];
			var jid = e.data[4];
			var func = eval(e.data[5]);
			
			// var returner = returnerHelper(parentid+workerid,jid);
			// func(returner);
			
			break;
	}
};

//TODO delete
console.log("up");