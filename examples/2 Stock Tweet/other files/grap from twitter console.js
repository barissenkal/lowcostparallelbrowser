var tweetDivs = document.getElementsByClassName("tweet");

var tweets = [];

for (var i = 0; i < tweetDivs.length; i++) {
	var tweetDiv = tweetDivs[i];
	try {
		var text = tweetDiv.getElementsByClassName("tweet-text")[0].innerText;
		var time = tweetDiv.getElementsByClassName("_timestamp")[0].getAttribute("data-time-ms");
		
		tweets.push({"text":text,"time":time});
	} catch (err){
		tweets.push({"err":err});
	}
}

JSON.stringify(tweets);