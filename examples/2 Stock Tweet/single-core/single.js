console.time("all");

var http = require("http");

function httpGetHelper(url,callback) {
	http.get(url, (res) => {
		const statusCode = res.statusCode;
		const contentType = res.headers['content-type'];

		let error;
		if (statusCode !== 200) {
			error = new Error(`Request Failed.\n` +
			                  `Status Code: ${statusCode}`);
		} else if (!/^application\/json/.test(contentType)) {
			error = new Error(`Invalid content-type.\n` +
			                  `Expected application/json but received ${contentType}`);
		}
		if (error) {
			console.log(error.message);
			// consume response data to free up memory
			res.resume();
			return;
		}

		res.setEncoding('utf8');
		let rawData = '';
		res.on('data', (chunk) => rawData += chunk);
		res.on('end', () => {
			try {
				//console.log(rawData);
				let parsedData = JSON.parse(rawData);
				callback(parsedData);
			} catch (e) {
				console.log(e.message);
			}
		});
	}).on('error', (e) => {
		console.log(`Got error: ${e.message}`);
	});
}


var largeChanges, firstReqLeftCount;
var totalRuns = 10;

var firstStep = function () {
	
	largeChanges = [];
	firstReqLeftCount = 10;
	for (var i = 0; i <= firstReqLeftCount; i++) {
		
		httpGetHelper("http://52.212.88.241:3000/stocks/?index="+i,function (rows) {
			
				var threshold = 1.2;

				var baseDateInSec,
					lastDateInSec,
					lastPrice;
					
				for (var i = 0; i < rows.length; i++) {
					var row = rows[i];
					var parts = row.split(",");
					
					if(isNaN(parts[0])){
						if(parts[0] == "PRE"){
							try{
								lastPrice = parseFloat(parts[1]);
								lastDateInSec = 0;
								continue;
							} catch (err) {
								log("BUG 1.1",err);
							}
						} else if(parts[0].indexOf("a") == 0 && parts[0].length > 2){
							try{
								baseDateInSec = parseInt(parts[0].substring(1));
								continue;
							} catch (err) {
								log("BUG 1.2",err);
							}
						} else {
							log("BUG 1.3");
						}
					}
					
					if(parts.length < 3) {
						log("BUG 2");
						continue;	
					}
					
					try {
						var closing = parseFloat(parts[1]);
						if (Math.abs(closing - lastPrice) > threshold) {
							var closingDateInSecs = baseDateInSec + parseInt(parts[0]);
							largeChanges.push({
								"price":closing,
								"lastPrice":lastPrice,
								"lastDateInSec":lastDateInSec,
								"dateInSec":closingDateInSecs
							});
						}
						lastPrice = closing;
					}  catch (err) {
						log("BUG 3",err);
					}
				}
				
				if(--firstReqLeftCount == -1){
					console.log("largeChanges",largeChanges);
					nextStep();
				}
			
		});
		
	}
}

var nextStep = function () {
	
	//Assumed to come with stock information.
	var keywords = ["$ba","boeing","stock","flight","airplane","nyse"];
	
	if (largeChanges.length == 0){
		console.log("BUUUUUUGGGG");
		return;
	}
	
	var results = [];
	var reqLeft = largeChanges.length;
	
	var matchingString = "";
	for (var i = 1; i < keywords.length; i++) {
		matchingString += '|\\b' + keywords[i] + '\\b';
	}
	var matcher = new RegExp(matchingString.substr(1));
	
	largeChanges.forEach(function (change) {
		/*
			change: {
				"price":...,
				"lastPrice":...,
				"lastDateInSec":...,
				"dateInSec":...
			}
		*/
		
		httpGetHelper("http://52.212.88.241:3000/tweets/?startSec=" + change.lastDateInSec + "&endSec=" + change.dateInSec,
		    function (data,err) {
		    	if (err) {
		    		results.push({"err":err});
		    	} else {
		    		for (var i = data.length - 1; i >= 0; i--) {
		    			
		    			var keywordFound = false;
		    			
	    				if(matcher.test(data[i].text.toLocaleLowerCase())){
	    					keywordFound = true;
	    				}
		    			
		    			if (keywordFound) {
		    				results.push({
								"price":change.price,
								"lastPrice":change.lastPrice,
								"lastDateInSec":change.lastDateInSec,
								"cateInSec":change.cateInSec,
								"text":data[i].text,
								"time": data[i].time
							});
			    		}
		    			
		    		}
		    	}
		    	if(--reqLeft == 0) {
		    		console.log("results:",JSON.stringify(results,null,2));
		    		if(--totalRuns == 0){
		    			console.timeEnd("all");
		    		} else {
		    			//console.log("totalRuns",totalRuns);
		    			firstStep();
		    		}
		    		
		    	}
			}
		)
		
	});
	
}



firstStep();