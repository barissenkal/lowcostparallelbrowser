function (parts,returner){

	var comb = [];
	
	for (var i = 0; i < parts[0].length; i++) {
		if(parts[0][i] != null) comb.push(parts[0][i]);
	}
	
	for (var j = 0; j < parts[1].length; j++) {
		if(parts[1][j] != null) comb.push(parts[1][j]);
	}
	
	comb.sort(function (a, b) {
		return b.time - a.time;
	});
	
	returner(comb);

}