function (inputs,returners,done,index,size,req){

	//Assumed to come with stock information.
	var keywords = ["$ba","boeing","stock","flight","airplane","nyse"];

	var toCheck = inputs["stocksData"];
	
	if (toCheck.length < 1){
		//Nothing to check case.
		returners["g3"](index,[]);
		done();
		return;
	}
	
	var results = [];
	var reqLeft = toCheck.length;
	
	var matchingString = "";
	for (var i = 1; i < keywords.length; i++) {
		matchingString += '|\\b' + keywords[i] + '\\b';
	}
	var matcher = new RegExp(matchingString.substr(1));
	
	toCheck.forEach(function (change) {
		/*
			change: {
				"price":...,
				"lastPrice":...,
				"lastDateInSec":...,
				"dateInSec":...
			}
		*/
		
		req.get({
			"query": {
					"startSec":change.lastDateInSec,
					"endSec":change.dateInSec,
				}
			},
		    function (data,err) {
		    	if (err) {
		    		results.push({"err":err});
		    	} else {
		    		for (var i = data.length - 1; i >= 0; i--) {
		    			
		    			var keywordFound = false;
		    			
	    				if(matcher.test(data[i].text.toLocaleLowerCase())){
	    					keywordFound = true;
	    				}
		    			
		    			if (keywordFound) {
		    				results.push({
								"price":change.price,
								"lastPrice":change.lastPrice,
								"lastDateInSec":change.lastDateInSec,
								"cateInSec":change.cateInSec,
								"text":data[i].text,
								"time": data[i].time
							});
			    		}
		    			
		    		}
		    	}
		    	if(--reqLeft == 0) {
		    		log("[sent done()]");
		    		returners["g3"](index,results);
					done();
		    	}
			}
		)
		
	});

}