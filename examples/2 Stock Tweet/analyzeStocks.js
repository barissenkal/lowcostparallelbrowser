function (rows,index,size,returner){

	var threshold = 1.2;

	var largeChanges = [];
	var baseDateInSec,
		lastDateInSec,
		lastPrice;
		
	for (var i = 0; i < rows.length; i++) {
		var row = rows[i];
		var parts = row.split(",");
		
		if(isNaN(parts[0])){
			if(parts[0] == "PRE"){
				try{
					lastPrice = parseFloat(parts[1]);
					lastDateInSec = 0;
					continue;
				} catch (err) {
					log("BUG 1.1",err);
				}
			} else if(parts[0].indexOf("a") == 0 && parts[0].length > 2){
				try{
					baseDateInSec = parseInt(parts[0].substring(1));
					continue;
				} catch (err) {
					log("BUG 1.2",err);
				}
			} else {
				log("BUG 1.3");
			}
		}
		
		if(parts.length < 3) {
			log("BUG 2");
			continue;	
		}
		
		try {
			var closing = parseFloat(parts[1]);
			if (Math.abs(closing - lastPrice) > threshold) {
				var closingDateInSecs = baseDateInSec + parseInt(parts[0]);
				largeChanges.push({
					"price":closing,
					"lastPrice":lastPrice,
					"lastDateInSec":lastDateInSec,
					"dateInSec":closingDateInSecs
				});
			}
			lastPrice = closing;
		}  catch (err) {
			log("BUG 3",err);
		}
	}
	
	returner(largeChanges);

}