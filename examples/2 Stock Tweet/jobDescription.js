var N = 10;

group("stocksData",{
  "reader":"analyzeStocks",
 "json":"http://52.212.88.241:3000/stocks/",
 "size": N
})

group("g3",{
 "ordered":true,
 "size":N
})

map({
 "inputs":["stocksData"],
 "func":"analyzeTweets",
 "outputs":["g3"],
 "reqBase":"http://52.212.88.241:3000/tweets/"
})

reduce({
  "input":"g3",
 "func":"reduceTogether",
 "result":"result1"
})
