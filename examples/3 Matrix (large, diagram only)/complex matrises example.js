var N = 3;

group("i1",{
 "size": N*N
})

group("i2",{
 "size": N*N
})

group("i3",{
 "size":N*N
})

group("o1",{
 "size":N*N
})

group("o2",{
 "size":N*N
})

group("o3",{
 "size":N*N
})

map({
 "inputs":["i1","i2"],
 "func":"f1",
 "outputs":["o1"]
})

map({
 "inputs":["i2","i3"],
 "func":"f2",
 "outputs":["o2"]
})

map({
 "inputs":["o1","o2"],
 "func":"f3",
 "outputs":["o3"]
})


reduce({
  "input":"o2",
 "func":"reduce1",
 "result":"result1"
})

reduce({
  "input":"o3",

 "func":"reduce2",
 "result":"result2"
})