var N = 9;

var I = 4;

var i;
for (i = 0; i < I; i++) {
	
	group("top"+i,{
	 "size": N
	})

	group("topRight"+i,{
	 "size": N
	})

	group("right"+i,{
	 "size": N
	})

	group("bottomRight"+i,{
	 "size": N
	})

	group("bottom"+i,{
	 "size": N
	})

	group("bottomLeft"+i,{
	 "size": N
	})

	group("left"+i,{
	 "size": N
	})

	group("topLeft"+i,{
	 "size": N
	})

	group("center"+i,{
	 "size": N
	})
	
}

for(i = 0; i<(I-1); i++){
    map({
 		"inputs":["top"+i,"topRight"+i,"right"+i,"bottomRight"+i,"bottom"+i,"bottomLeft"+i,"left"+i,"topLeft"+i,"center"+i],
 		"func":"map"+i,
 		"outputs":["top"+(i+1),"topRight"+(i+1),"right"+(i+1),"bottomRight"+(i+1),"bottom"+(i+1),"bottomLeft"+(i+1),"left"+(i+1),"topLeft"+(i+1),"center"+(i+1)]
	})
}


reduce({
  "input":"top"+i,
 "func":"redt",
 "result":"top"
})

reduce({
  "input":"topRight"+i,
 "func":"redtr",
 "result":"topRight"
})

reduce({
  "input":"right"+i,
 "func":"redr",
 "result":"right"
})

reduce({
  "input":"bottomRight"+i,
 "func":"redbr",
 "result":"bottomRight"
})

reduce({
  "input":"bottom"+i,
 "func":"redb",
 "result":"bottom"
})

reduce({
  "input":"bottomLeft"+i,
 "func":"redbl",
 "result":"bottomLeft"
})

reduce({
  "input":"left"+i,
 "func":"redl",
 "result":"left"
})

reduce({
  "input":"topLeft"+i,
 "func":"redtl",
 "result":"topLeft"
})

reduce({
  "input":"center"+i,
 "func":"redc",
 "result":"center"
})