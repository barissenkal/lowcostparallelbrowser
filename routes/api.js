var debug = require('debug')('lcccmb:api');
var express = require('express');
var multer = require('multer');
var parseFunction = require('parse-function');
var fs = require('fs');
var path = require('path');
var extend = require('util')._extend;

var db = require('../db_related/dynamo.js');

//TODO find a json parser that accepts plain numbers. (can't post a single number)

//db.init();

debug("DB:"+db.name);

var router = express.Router();
var upload = multer(); // for parsing multipart/form-data

/* TODO for division work package;
 * Have couple of default options (gird like stuff) and a function file option.
 * First check js file, then create a require(...) format version of it and load that.
 */
 
//TODO update when structure changes.
// var jobConfig = {
// 	"defaultArgs": "returner", //TODO update when client code changes.
// 	"overwriteText":"",
// 	"data":null,
// 	"dataOriginalString":null,
// 	"taskBody":null
// }

//Before everything, let's get job configuration
//TODO? this should be done beforehand in real life, maybe with a page for sending jobs.
try {
	
	var functions = {};
	var JSONs = {};
	
	//TODO update these words for WebWorker
	var bannedWords = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'config', 'js_ban.json'), 'utf8'));
	var overwriteWords = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'config', 'js_overwrite.json'), 'utf8'));
	
} catch (e) {
	console.error(e);
}

var makeVarNameSafe = function (name) {
	// TODO
	return name;
}

//TODO use submission ID.
var processFunc = function(submissionID,funcName,data){

	var parsed = parseFunction(data);
	
	for(var i in bannedWords){
		//TODO find a better way to do this.
		if(parsed.body.indexOf(bannedWords[i]) >= 0){
			console.log("\x1b[41m "+funcName+" has banned word:"+bannedWords[i]+"\x1b[0m");
		}
	}
	
	var tempOverwrite = "";
	for(var i in overwriteWords){
		//TODO find a better way to do this.
		if(parsed.body.indexOf(overwriteWords[i]) >= 0){
			tempOverwrite += overwriteWords[i] + ",";
		}
	}
	
	var overwriteText = "";
	if(tempOverwrite.length > 0) {
		tempOverwrite = tempOverwrite.substring(0, tempOverwrite.length -1);
		console.log("\x1b[43m\x1b[30mOverwriting ("+funcName+") :"+tempOverwrite+"\x1b[0m\x1b[0m");
		overwriteText = "var " + tempOverwrite + ";\n";
	}
	
	if(!(submissionID in functions)){
		functions[submissionID] = {};
	}
	functions[submissionID][funcName] =  "var " + makeVarNameSafe(funcName) + " = function (" + parsed.args + ") {" + overwriteText + parsed.body + "};";
	
}

//TODO use submission ID.
var gatherData = function(submissionID,fileName,data) {
	console.log("fileName,data",fileName,data);
	if(!(submissionID in JSONs)){
		JSONs[submissionID] = {};
	}
	JSONs[submissionID][fileName] = data;//JSON.parse(data);
}

var findNodeInNodes = function(nodes,id){
	for (var k in nodes){
		if(nodes[k].id == id){
			return nodes[k];
		}
	}
	console.log("BUG in findNodeInNodes");
	return {};
}

/* Submit Job */
router.post('/submit/:id/', upload.array(), function(req, res, next) {
	var submissionID = req.params.id;
	var token = req.query.token;
	var data = req.body;
	
	if(token && token == "1234567890"){ //TODO have some kind of authentication. *****
		//Send status when everything is done or just send it ?
		res.sendStatus(200);
	} else {
		res.sendStatus(400);
		console.log("Wrong token", new Date());
		return;
	}
	
	//TODO check if submissionID exists in db.
	
	var job;
	
	if("jobDescription" in data){
		job = data["jobDescription"];
	} else {
		console.error("Error no jobDescription");
		return;
	}
	
	if("funcs" in data){
		for (var k in data["funcs"]) {
			//TODO use submission ID.
			processFunc(submissionID,k,data["funcs"][k]);
		}
	} else {
		throw "Error no funcs";
	}
	
	if("JSONs" in data){
		for (var k in data["JSONs"]) {
			//TODO use submission ID.
			gatherData(submissionID,k,data["JSONs"][k]);
		}
	} else {
		throw "Error no JSONs";
	}
	
	if("nodes" in job && job.nodes.length){
		
		// Prep nodes.
		job.nodes.forEach(function(node) {
			
			switch(node.type){
				case 0:
					var groupid = node.id;
					if("reader" in node && "json"){
						//processFunc(node.reader);
						//gatherData(node.json);
						node.priority = 0;
					}
					break;
				case 1:
					//TODO
					break;
				case 2:
					//TODO
					break;
				case 3:
					var funcName = node.id;
					
					//processFunc(funcName);
					
					//TODO make sure map's input sizes match.
					var size = findNodeInNodes(job.nodes,node.inputs[0]).size;
					node.size = size;
					//node.ordered = true; //what is this?
					
					//TODO calculate priority
					var priority = 0;
					for (var i = 0; i < node.inputs.length; i++) {
						
						var inputNode = findNodeInNodes(job.nodes,node.inputs[i]);
						
						if(! ("usedAt" in inputNode)) {
							inputNode.usedAt = [];
						}
						inputNode.usedAt.push(node.id);
						
						priority += inputNode.priority + 1;
					}
					
					for (var i = 0; i < node.outputs.length; i++) {
						findNodeInNodes(job.nodes,node.outputs[i]).priority = priority;
					}
					
					node.priority = priority;
					
					break;
				case 4:
					var inputGroup = findNodeInNodes(job.nodes,node.input);
					node.size = inputGroup.size;
					node.priority = inputGroup.priority + 1;
					
					if(! ("usedAt" in inputGroup)) {
						inputGroup.usedAt = [];
					}
					inputGroup.usedAt.push(["reduce",node.id,node.result].join("-"));
					
					var funcName = node.id;
					
					//processFunc(funcName);
					
					break;
				case 5:
					//Only for visual
					break;
				default:
					console.log("Unknown node");
			}
			
		});
		
		//TODO use submission ID.
		//Throw in DB
		job.nodes.forEach(function(node) {
			
			switch(node.type){
				case 0:
					var groupid = node.id;
					if("reader" in node && "json"){
						db.throwDataPartsIn({
							"subID":submissionID,
							"group":groupid,
							"usedAt":node.usedAt,
							"size":node.size
						},function(){
							db.throwReaderJobsIn({
								"subID":submissionID,
								"func":node.reader,
								"json":node.json,
								"group":groupid,
								"priority":0,
								"size":node.size
							});
						});
					} else {
						db.throwDataPartsIn({
							"subID":submissionID,
							"group":groupid,
							"usedAt":node.usedAt,
							"size":node.size
						});
					}
					break;
				case 1:
					//TODO
					break;
				case 2:
					//TODO
					break;
				case 3:
					var funcName = node.id;
					
					db.throwMapJobsIn({
						"subID":submissionID,
						"func":funcName,
						"size":node.size,
						"inputGroups":node.inputs,
						"outputGroups":node.outputs,
						"priority":node.priority,
						"reqBase":node.reqBase
					});
					
					break;
				case 4:
					var funcName = node.id;
					
					db.throwReduceJobsIn({
						"subID":submissionID,
						"func":funcName,
						"initSize":node.size,
						"inputGroup":node.input,
						"resultName":node.result,
						"priority":node.priority
					});
					
					break;
				case 5:
					//Only for visual
					break;
				default:
					console.log("Unknown node");
			}
			
		});
		
	} else {
		throw "Error: no nodes in job";
	}
	
});


/* GET js function */
router.get('/js/:funcname/', upload.array(), function(req, res, next) {
	var funcname = req.params.funcname;
	var submissionID = req.query.subID;
	
	debug("get funcname = " + funcname + " subID=" + submissionID);
	
	//TODO get config from server
	
	if(submissionID in functions && funcname in functions[submissionID]){
		var js = functions[submissionID][funcname];
		res.setHeader('content-type', 'text/javascript');
		res.end(js);
	} else {
		console.log("wrong function name or subID");
		res.sendStatus(404);
	}
	
});

/* GET JSON */
router.get('/json/:fileName/', upload.array(), function(req, res, next) {
	var fileName = req.params.fileName;
	var submissionID = req.query.subID;
	
	debug("get JSON file = " + fileName+ " subID=" + submissionID);
	
	//TODO get config from server
	
	if (submissionID in JSONs && fileName in JSONs[submissionID]) {
		var json = JSONs[submissionID][fileName];
		res.send(json);
	} else {
		debug("wrong file name");
		console.log("wrong file name or subID");
		res.sendStatus(404);
	}
	
});

/* GET group part */
router.get('/g/:gname/:part/', upload.array(), function(req, res, next) {
	var jid = req.query.jid;
	var submissionID = req.query.subID;
	var gname = req.params.gname;
	var part = req.params.part;
	debug("get group:"+gname+" part:" + part);
	
	//TODO maybe use jid? get multiple parts together?
	
	db.getDataPart({"group":gname,"part":part,"jid":jid,"subID":submissionID},function(data,err) {
		//TODO error check
		if(err){
			res.sendStatus(404);
		} else {
			res.json(data);
		}
	})
	
});

/* POST group part */
router.post('/g/:gname/:part/', upload.array(), function(req, res, next) {
	var gname = req.params.gname;
	var part = req.params.part;
	var data = req.body;
	var jid = req.query.jid;
	var submissionID = req.query.subID;
	debug("post group:"+gname+" part:" + part + " data = ",data + " jid = " + jid + " subID=" +submissionID);
	
	res.sendStatus(200);
	
	//TODO check data before sending to db

	//Put data back to DB
	db.sendDataPart({"group":gname,"part":part,"data":data,"jid":jid, "subID":submissionID});
	
});

/* GET result part */
router.get('/r/:result/:partID', upload.array(), function(req, res, next) {
	var submissionID = req.query.subID;
	var resultName = req.params.result;
	var partID = req.params.partID;
	var jid = req.query.jid;
	debug("get resultName:"+resultName+" jid:" + jid + " partID:" + partID + "subID:" + submissionID);
	
	db.getResultPart({"resultName":resultName,"jid":jid,"partID":partID,"subID":submissionID},function(data,err) {
		//TODO error check
		console.log("getResultPart",data,err);
		if(err){
			res.sendStatus(400);
		} else {
			res.json(data);	
		}
	})
});

/* POST result part */
router.post('/r/:result/', upload.array(), function(req, res, next) {
	var submissionID = req.query.subID;
	var resultName = req.params.result;
	var data = req.body;
	var jid = req.query.jid;
	debug("get resultName:"+resultName + " data = ",data);
	
	//TODO check if jid needed?
	
	res.sendStatus(200);
	
	//TODO check data before sending to db

	//Put data back to DB
	db.sendResultPart({"subID":submissionID,"resultName":resultName,"data":data,"jid":jid});
	
});


function clientID(cid) {
	
	if (cid) return cid;
	
	//gnu id generator
	return 'xxx-xxxxx-xxxxx-xxxxx'.replace(/[xy]/g, function (c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}

/* GET init new job */
router.get('/', upload.array(), function(req, res, next) {
	var cid = req.query.cid;
	debug("init with cid="+cid);
	
	var info = {
		"cid":clientID(cid), //TODO get from db
	};
	
	db.getPriorityJob({},function (job,err) {
		
		if(err){
			//if no job, return ms as timeout attribute
			info.timeout = 2000;//5000
		} else {
			
			//delete job.status;
			//delete job.priority;
			
			info.job = job;
			
			//get inputs etc from db (or just give json from here.).
			
			switch(job.jobType) {
				case "map":
				
					//delete job.partsLeft;
					
					break;
				case "read":
				
					break;
				case "reduce":
					
					break;
				case "reduceInit":
					
					break;
				default:
					console.log("Error: Unknown job type");
			}
			
			//TODO save client, job start time and jobid in db.
			
			console.log(job);
			
		}
		
		res.json(info);
		
	});
	
});

//TODO special finish for reader jobs ???

/* GET finish job */
router.get('/done/', upload.array(), function(req, res, next) {
	var submissionID = req.query.subID;
	var jid = req.query.jid;
	var cid = req.query.cid;
	debug("job done, jid="+jid+" cid="+cid);
	
	db.finishJob({"subID":submissionID,"jid":jid,"cid":cid});
	
	res.sendStatus(200);
	
});


/* GET throwJobsBackToPool */
router.get('/progress/:subID/', upload.array(), function(req, res, next) {
	var submissionID = req.params.subID;
	
	if(submissionID){
		db.getSubmissionProgress({"subID":submissionID},function(data,err) {
			if(err){
				res.json({"errorMessage":err});
			} else if (data.total == 0){
				res.json({"errorMessage":"No jobs submitted yet"});
			} else {
				res.json(data);
			}
		});
	} else {
		res.json({"errorMessage":"No submission ID"});
	}
	
});

/* GET Result: Just for the demo*/
router.get('/result/:result/', function(req, res, next) {
	var submissionID = req.query.subID;
	var resultName = req.params.result;
	debug("get finished resultName:"+resultName);
	
	db.getFinalResult({"subID":submissionID,"resultName":resultName},function(data,err) {
		
		//TODO check for error
		
		if(err){
			res.json({"errorMessage":err});
		} else {
			//TODO change to message of some sort
			res.json(data);
		}
		
	});
	
});

module.exports = router;
