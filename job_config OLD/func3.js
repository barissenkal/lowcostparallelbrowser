function (inputs,returners,done,index,size){
	var N = Math.sqrt(size);
	var m = index % N;
	var n = (index - m) / N;
	
	var a = inputs["g1"];
	var b = inputs["g2"];
	
	var total = 0;
	
	for (var i = 0; i < N; i++) {
		total += a[i] * b[i];
	}
	
	returners["g3"](index,[{"n":n,"m":m,"val":total}]);
	done();
}