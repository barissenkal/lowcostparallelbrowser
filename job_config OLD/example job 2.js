var N = 3;

group("g1",{
	"reader":"func1",
	"json":"m1.json",
	"size": N*N
})

group("g2",{
	"reader":"func2",
	"json":"m2.json",
	"size": N*N
})

group("g3",{
	"ordered":true,
	"size":N*N
})

group("g4",{
	"ordered":true,
	"size":N*N
})


map({
	"inputs":["g1"],
	"func":"func5",
	"outputs":["g4"]
})

map({
	"inputs":["g2","g4"],
	"func":"func3",
	"outputs":["g3"]
})

reduce({
	"input":"g3",
	"func":"func4",
	"result":"result1"
})

