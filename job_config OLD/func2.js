function (json,index,size,returner){
	var N = Math.sqrt(size);
	var m = index % N;
	var n = (index - m) / N;
	var array = [];
	
	for (var i = 0; i < N; i++) {
		array.push(json[i][m]);
	}
	
	returner(array);
}