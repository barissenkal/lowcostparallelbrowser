
/* Job size variable can be spesified at submission time */
var N = config()["job_size"];

/* Group is smt like a distributed array.
 * "ordered" means reduce will be done with n, n+1 like pairs.
 * "size" only matters if group is input for another map or when it is ordered.
 * Client will spesify part number when sending results back.
 * Warning: if two clients send a result for same part, only latest one sent is used.
 */
group("g1",{
	"ordered":true
	"size": N
});

group("g2",{
	"size": N
});

/* Group can also have no spesified size.
 * This will make next step wait until all map operations that output this group
 * finish.
 * When clients are sending data back, instead of part number they will use
 * keywords to sub-group values.
 */
group("g3",{
	"size": -1
});

/* Single is single variable that can be updated atomically.
 * "op" spesifies update operation.
 * Single variables are not updated in clients, only sent once to client
 * and each client can update its value only once.
 */
single("s1",{
	"op":"inc"
});

/* Split can use a "func"tion to split a json file into "size" many pieces
 * and creates a ordered group. Has to spesifly number of pieces for func.
 * If there is a single json, and N size, json is sent to N clients to greate g4
 */
group("g4",{
	"type":"custom"
	"func":"func1"
	"json":"data.json",
	"size": N,
})

/* Split can also work with X many items coming from multiple files
 */
var X = group("g5",{
	"type":"array",
	"json":["d1.json","d2.json","d3.json"]
})

//TODO client should be able to download files (GET request. JSON only.)

/* Split can also work with Y many files downloaded at client.
 */
var Y = group("g6",{
	"type":"one2one",
	"json":["http://example.com/d1.json","http://example.com/d2.json","http://example.com/d3.json","...","http://example.com/dY.json"],
	"func":"func2"
})

/* Range generates a array with size (end - start) / step */
range("g7",{
	"start":0,
	"end": 4 * N,
	"step": 2
})

/*
 * Map runs the "func" in client with inputs and sends updates for outputs.
 * Output g2 uses keywords, output g3 uses part numbers.
 * "func" can send multiple updates to g2 and g3 but only once to s1.
 */
map({
	"inputs": ["g4","g5"],
	"func":"func3",
	"outputs":["s1","g2","g3"]
})

/* Reduce can work with groups with part numbers and keywords.
 * "func" here has to be type (alpha, alpha) -> alpha
 * reduce will combine each keyword seperately with log(items in keyword) steps.
 */
 
reduce({
	"input":"g3",
	"func":"func4",
	"result":"result1"
})


/* Reduce will combine g2 in log(#parts) steps.
 * parts spesify number of inputs in "func".
 * In here func5 is: (alpha, alpha, alpha) -> alpha
 * 
 */

reduce({
	"input":"g2",
	"parts":3
	"func":"func5",
	"result":"result2"
})


/* Reduce can be used to resize ordered groups as well.
 * "reduce" below will get two g7s (in order since g7 is ordered),
 * run "func" on them to combine into g8.
 */

group("g8",{
	"size": N
});

reduce({
	"input":"g7",
	"func":"func6"
	"times":1,
	"into":"g8"
})






